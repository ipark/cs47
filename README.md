CS47. Introduction to Computer Systems (Fall2018 @SJSU)
=========

Textbook: Computer Organization and Design: The Hardware/Software Interface (4th ed.) David A. Patterson and John L. Hennessy. 2011. 
---------

Topics:
-------

Number Representation (Integer Representation, Format conversion, Negative Integer Representation) 

Assembler, Linker, Loader (Macros)

SPIM Simulator (Simulator, System Calls, Simple Program, Macro usage)

Memory Usage I (Registers)

Memory Usage II (Memory Model)

Memory Usage III (Memory, Stack Operation, Global/Local Variable)

Procedures Calls (High/Low Level Procedure Call, Caller Run Time Environment (RTE), RTE Storage)

Boolean Algebra I (Boolean Values, Operations, Functions, Truth Table, Basic Identities & Algebraic Manipulation)

Boolean Algebra II (Standard Forms, Karnaugh Map)

Logic Gates (Combinational Logic Gates)

Logic Circuit Design (Combinational Circuit Design)

Digital Circuit Components

Addition-Subtraction Circuit (Binary Addition/Subtraction Operations, Logic Add/Sub in Assembly)

Multiplication (Binary Multiplication Curcuit, Algorithm for Unsigned and Signed Cases)

Division (Binary Division Curcuit, Algorithm for Unsigned and Signed Cases)

