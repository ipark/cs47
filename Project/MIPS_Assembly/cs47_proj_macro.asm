# Add you macro definition here - do not touch cs47_common_macro.asm"
#<------------------ MACRO DEFINITIONS ---------------------->#

#===================================================
# CS47-Fall2018-Project - INHEE PARK (Due 12/3/2018)
#===================================================

# Macro 'extract_nth_bit' to extract nth bit from a bit pattern.
# .macro extract_nth_bit($regDestination, $regSrcBitPattern, $regBitPosition)
#   $regDestination 
#   $regSrcBitPattern
#   $regBitPosition: 0-31
#
# TEST
# li $t0, 0x1
# ori $s2, $zero, 0x6F # 0b1101111 (4th bit is 0)
# li $t1, 4
# extract_nth_bit($t2, $s2, $t1) ==> $t2 = 0
.macro extract_nth_bit($regDestination, $regSrcBitPattern, $regBitPosition)
 # regDestination = regSrcBitPattern >> regBitPosition
 srlv $regDestination, $regSrcBitPattern, $regBitPosition 
 # regDestination = regDestination & 0x1 
 andi $regDestination, $regDestination, 0x1               
.end_macro

#
#Macro 'insert_to_nth_bit' to insert bit 1 at nth bit to a bit pattern.
# .macro insert_to_nth_bit($regDestination, $regBitPosition, $regSourceBit, $regTempMask)
#   $regDestination   
#   $regBitPosition: 0-31
#   $regSourceBit
#   $regTempMask
#
# TEST
# ori $t0, $zero, 0x6F # 0b1101111 (4th bit is 0)
# li $s2, 4
# li $t1, 0x1
# insert_to_nth_bit($t0, $s2, $t1, $t9)
.macro insert_to_nth_bit($regDestination, $regBitPosition, $regSourceBit, $regTempMask)
 li  $regTempMask, 1
 # regTempMask = 1 << regBitPosition
 sllv $regTempMask, $regTempMask, $regBitPosition  
 not  $regTempMask, $regTempMask # invert bit
 # mask regDestination with regTempMask
 and  $regDestination, $regDestination, $regTempMask 
 # regSourceBit = regSourceBit << regBitPosition
 sllv $regSourceBit, $regSourceBit, $regBitPosition 
 # regDestination = regDestination | regSourceBit 
 or   $regDestination, $regDestination, $regSourceBit  
.end_macro

