.include "./cs47_proj_macro.asm"
.text
.globl au_logical

#===================================================
# CS47-Fall2018-Project - INHEE PARK (Due 12/3/2018)
#===================================================
#
# TBD: Complete your project procedures
# Needed skeleton is given
#####################################################################
# Procedure 'au_logical'
# Argument:
#   $a0: First number
#   $a1: Second number
#   $a2: operation code ('+':add, '-':sub, '*':mul, '/':div)
# Return:
#    $v0: ($a0+$a1) | ($a0-$a1) | ($a0*$a1):LO | ($a0/$a1)
#    $v1:           |           | ($a0*$a1):HI | ($a0%$a1)
#####################################################################
au_logical:
  #store RTE - (2 + 3) * 4 = 20 bytes
  addi  $sp, $sp, -24
  sw  $fp, 24($sp)
  sw  $ra, 20($sp)
  sw  $a0, 16($sp) # first number
  sw  $a1, 12($sp) # second number
  sw  $a2,  8($sp) # mode
  #--------------
  beq $a2, '+', add_logical
  beq $a2, '-', sub_logical
  beq $a2, '*', mul_signed
  beq $a2, '/', div_signed
  #--------------
end_au_logical:
  #restore RTE
  lw  $fp, 24($sp)
  lw  $ra, 20($sp)
  lw  $a0, 16($sp)
  lw  $a1, 12($sp)
  lw  $a2,  8($sp)
  addi  $sp, $sp, 24
  jr $ra

#####################################################################
#######  ADDITION / SUBTRACTION #####################################
#####################################################################
# Procedure 'add_logical'
# Argument:
#   $a0: First number
#   $a1: Second number
# Return:
#   $a0 + $a1 in $v0
# Call:
#   add_sub_logical with $a2 = 0x00000000
#####################################################################
add_logical:
  #store RTE - (2 + 3) * 4 = 20 bytes
  addi  $sp, $sp, -24
  sw  $fp, 24($sp)
  sw  $ra, 20($sp)
  sw  $a0, 16($sp) # first number
  sw  $a1, 12($sp) # second number
  sw  $a2,  8($sp) # mode
  #--------------
  li $a2, 0 # $a2=0x00000000
  jal add_sub_logical
  j end_add_logical
  #--------------
end_add_logical:
  #restore RTE
  lw  $fp, 24($sp)
  lw  $ra, 20($sp)
  lw  $a0, 16($sp)
  lw  $a1, 12($sp)
  lw  $a2,  8($sp)
  addi  $sp, $sp, 24
  jr $ra
  
#####################################################################
# Procedure 'sub_logical'
# Argument:
#   $a0: First number
#   $a1: Second number
#   $a2: Mode
# Return:
#   $a0 - $a1 in $v0
# Call:
#   add_sub_logical with $a2 = 0xFFFFFFF
#####################################################################
sub_logical:
  #store RTE - (2 + 3) * 4 = 20 bytes
  addi  $sp, $sp, -24
  sw  $fp, 24($sp)
  sw  $ra, 20($sp)
  sw  $a0, 16($sp) # first number
  sw  $a1, 12($sp) # second number
  sw  $a2,  8($sp) # mode
  #--------------
  li $a2, -1      # $a2 = 0xFFFFFFFF
  not $a1, $a1    # invert to convert 2's complement
  jal add_sub_logical
  j end_sub_logical
  #--------------
end_sub_logical: 
  #restore RTE
  lw  $fp, 24($sp)
  lw  $ra, 20($sp)
  lw  $a0, 16($sp)
  lw  $a1, 12($sp)
  lw  $a2,  8($sp)
  addi  $sp, $sp, 24
  jr $ra

#####################################################################
# Procdeure 'add_sub_logical'
# Argument:
#   $a0 : First number
#   $a1 : Second number
#   $a2 : Mode
#         0x00000000 is addition
#         0xFFFFFFFF is subtraction
# Return:
#   $v0: $a0 + $a1 in addition mode 
#        $a0 - $a1 in subtraction mode
#   $v1: (Upgraded) final carryout 
#####################################################################
add_sub_logical:
  #store RTE - (2 + 12) * 4 = 56 bytes
  addi  $sp, $sp, -56
  sw  $fp, 56($sp)
  sw  $ra, 52($sp)
  sw  $a0, 48($sp) # first number
  sw  $a1, 44($sp) # second number
  sw  $a2, 40($sp) # mode
  sw  $s0, 36($sp) # I
  sw  $s1, 32($sp) # C
  sw  $s2, 28($sp) # a0[index]
  sw  $s3, 24($sp) # a1[index]
  sw  $s4, 20($sp) # a0(xor)a1
  sw  $s5, 16($sp) # a0&a1
  sw  $s6, 12($sp) # Y
  sw  $s7,  8($sp) # S
  addi  $fp, $sp, 56
  #--------------
  li $s0, 0     # s0 = I = 0 
  li $s7, 0     # s7 = S = 0 
  extract_nth_bit($s1, $a2, $s0) # s1 = C = a2[s0]

for_loop_add_sub:
  extract_nth_bit($s2, $a0, $s0) # s2 = a0[s0]
  extract_nth_bit($s3, $a1, $s0) # s3 = a1[s0]
  xor $s4, $s2, $s3 # s4= A xor B
  and $s5, $s2, $s3 # s5= A.B
  xor $s6, $s1, $s4 # s6= Y = C xor (A xor B) 
  and $t7, $s1, $s4 # t7=     C.(A xor B)
  or $s1, $t7, $s5  # s1= C = C.(A xor B) + A.B 
  insert_to_nth_bit($v0, $s0, $s6, $t9) # $v0=$s7=S
  addi $s0, $s0, 1  # I++
  blt $s0, 32, for_loop_add_sub  # (I==32)?:end,contiue;
  # upgrade add_sub_logical to return final carryout (C=s1) in $v1
  move $v1, $s1
  #--------------
end_add_sub_logical:
  #restore RTE
  lw  $fp, 56($sp)
  lw  $ra, 52($sp)
  lw  $a0, 48($sp)
  lw  $a1, 44($sp)
  lw  $a2, 40($sp)
  lw  $s0, 36($sp)
  lw  $s1, 32($sp)
  lw  $s2, 28($sp)
  lw  $s3, 24($sp)
  lw  $s4, 20($sp)
  lw  $s5, 16($sp)
  lw  $s6, 12($sp)
  lw  $s7,  8($sp)
  addi $sp, $sp, 56
  jr $ra

#####################################################################
#######  MULTIPLICATION #############################################
#####################################################################
# Procedure 'twos_complement'
# Argument:
#   $a0 : Number of which 2's complement to be computed
# Return:
#   $v0 : Two's complement of $a0
# Note:
#   Use 'add_logical' and 'not' to compute '~$a0 + 1'
#####################################################################
twos_complement:
  #store RTE - (2 + 1) * 4 = 12 bytes
  addi  $sp, $sp, -16
  sw  $fp, 16($sp)
  sw  $ra, 12($sp)
  sw  $a0,  8($sp) # first number to be 2's complement
  #--------------
  not $a0, $a0        # $a0 : inverted $a0; first #
  li $a1, 1           # $a1 : second #
  jal add_logical     # $a0 = ~$a0 + 1
  j end_twos_complement
  #--------------
end_twos_complement:
  #restore RTE
  lw  $fp, 16($sp)
  lw  $ra, 12($sp)
  lw  $a0,  8($sp)
  addi $sp, $sp, 16
  jr $ra

#####################################################################
# Procedure 'twos_complement_if_neg'
# Argument:
#   $a0 : Number of which 2's complement to be computed
# Return:
#   $v0 : Two's complement of $a0 (if $a0 is negative)
# Note:
#   Test if $a0 < 0, then use 'twos_complement'
#####################################################################
twos_complement_if_neg:
  #store RTE - (2 + 1) * 4 = 12 bytes
  addi  $sp, $sp, -16
  sw  $fp, 16($sp)
  sw  $ra, 12($sp)
  sw  $a0,  8($sp) # first number to be 2's complement
  #--------------
  bgt $a0, $zero, pass # if $a0 > 0, just pass $a0 to $v0
  jal twos_complement
  j end_twos_complement_if_neg
pass:
  move $v0, $a0
  #--------------
end_twos_complement_if_neg:
  #restore RTE
  lw  $fp, 16($sp)
  lw  $ra, 12($sp)
  lw  $a0,  8($sp)
  addi $sp, $sp, 16
  jr $ra

#####################################################################
# Procedure 'twos_complement_64bit'
# Argument:
#     $a0 : Lo of the number
#     $a1 : Hi of the number
# Return:
#     $v0 : Lo part of 2's complemented 64 bit
#     $v1 : Hi part of 2's complemented 64 bit
# Note:
#     Upgrade add_logical to return final carryout in $v1
# Steps 
#     1. Invert both $a0, $a1.
#     2. Use add_logical to add 1 to $a0
#     3. Use add_logical to add carry from previous step to $a1
#####################################################################
twos_complement_64bit:
  #store RTE - (2 + 5) * 4 = 28 bytes
  addi  $sp, $sp, -32
  sw  $fp, 32($sp)
  sw  $ra, 28($sp)
  sw  $a0, 24($sp) # Lo of the number
  sw  $a1, 20($sp) # Hi of the number
  sw  $s0, 16($sp) # s0 <- a0
  sw  $s1, 12($sp) # s1 <- a1
  sw  $s2,  8($sp) # s3 <- v0 from LO add_logical with ~a0 + 1
  #--------------
  move $s0, $a0       # safe store of a0 to s0
  move $s1, $a1       # safe store of a1 to s1
  not $a0, $a0        # $a0: inverted $a0; 1st #
  li  $a1, 1          # $a1: 2nd # 
  jal add_logical     # LO result in $v0, $v1
  move $s2, $v0       # $s2: $v0 from LO ~$a0 + 1
  move $a0, $v1       # $a0: carryout from LO ~$a0 + 1; 1st #
  move $a1, $s1       # a1 <- s1
  not $a1, $a1        # $a1: inverted $a1; 2nd #
  jal add_logical     # HI result in $v0, $v1
  move $v1, $v0       # $v1 : Hi part of 2's complemented 64 bit
  move $v0, $s2       # $v0 : Lo part of 2's complemented 64 bit
  #--------------
end_twos_complement_64bit:
  #restore RTE
  lw  $fp, 32($sp)
  lw  $ra, 28($sp)
  lw  $a0, 24($sp)
  lw  $a1, 20($sp)
  lw  $s0, 16($sp)
  lw  $s1, 12($sp)
  lw  $s2,  8($sp)
  addi $sp, $sp, 32
  jr $ra

#####################################################################
# Procedure 'bit_replicator' 
# two replicate given bit value to 32 times
# Argument:
#     $a0 : 0x0 or 0x1 (the bit value to be replicated)
# Return:
#     $v0 : 0x00000000 if $a0 = 0x0
#           0xFFFFFFFF if $a0 = 0x1
#####################################################################
bit_replicator:
  #store RTE - (2 + 1) * 4 = 12 bytes
  addi  $sp, $sp, -16
  sw  $fp, 16($sp)
  sw  $ra, 12($sp)
  sw  $a0,  8($sp) # first number to be 2's complement
  #--------------
  beq $a0, $zero, allZero
  li $v0, -1 # 0xFFFFFFFF  
  j end_bit_replicator
allZero:
  li $v0, 0  # 0x00000000
  #--------------
end_bit_replicator:
  #restore RTE
  lw  $fp, 16($sp)
  lw  $ra, 12($sp)
  lw  $a0,  8($sp)
  addi $sp, $sp, 16
  jr $ra

#####################################################################
# Procedure 'mul_unsigned'
# Argument:
#     $a0 : Multiplicand
#     $a1 : Multiplier
# Return:
#     $v0 : Lo part of result
#     $v1 : Hi part of result
# Note:
#   -Use replication procedure to replicate 
#    single bit value to 32 bits to determine R. 
#   -Use mask to determine H[0] and then 
#    use 'insert_to_nth_nit' macro to insert this bit into L[31].
# Steps:
#   1. I = 0, H = 0, L = MPLR, M = MCND
#   2. R = {32{L[0]}}
#   3. X = M & R
#   4. H = H + X
#   5. L = L >> 1
#   6. L[31] = H[0]
#   7. H = H >> 1
#   8. I = I + 1
#   9. I == 32 ? : end, goto 2
#####################################################################
mul_unsigned:
  #store RTE - (2 + 10) * 4 = 48 bytes
  addi  $sp, $sp, -52
  sw  $fp, 52($sp)
  sw  $ra, 48($sp)
  sw  $a0, 44($sp) # $a0 : Multiplicand
  sw  $a1, 40($sp) # $a1 : Multiplier
  sw  $s0, 36($sp) # I
  sw  $s1, 32($sp) # H
  sw  $s2, 28($sp) # L=MPLR
  sw  $s3, 24($sp) # M=MCND
  sw  $s4, 20($sp) # R={32{L[0]}}
  sw  $s5, 16($sp) # X=M&R
  sw  $s6, 12($sp) # bit position 31 for L31
  sw  $s7,  8($sp) # H0
  addi  $fp, $sp, 52
  #--------------
  # step 1. I = 0, H = 0, L = MPLR, M = MCND
  li $s0, 0     # s0 = I = 0
  li $s1, 0     # s1 = H = 0
  move $s2, $a1 # s2 = L = MPLR  
  move $s3, $a0 # s3 = M = MCND

for_loop_mul:
  # step 2. R = {32{L[0]}}
  extract_nth_bit($s4, $s2, $zero) # s4 = L[0] = s2[0]
  move $a0, $s4  # to use bit_replicator with $a0 
  jal bit_replicator # output $v0
  move $s4, $v0  # s4 = R = {32{L[0]}}

  # step 3. X = M & R
  and $s5, $s3, $s4  # s5 = X

  # step 4. H = H + X // add_logical
  move $a0, $s1 # H, first number
  move $a1, $s5 # X, second number
  jal add_logical
  move $s1, $v0 # s1 = H
  
  # step 5. L = L >> 1
  srl $s2, $s2, 1    # s2 = L

  # step 6. L[31] = H[0]
  li $s6, 31  # s6 = 31
  extract_nth_bit($s7, $s1, $zero)  # s7 = H[0]
  insert_to_nth_bit($s2, $s6, $s7, $t9) # L[31] = H[0]
  #move $s2, $v0 # s2 = L = v0

  # step 7. H = H >> 1
  srl $s1, $s1, 1    # s1 = H

  # step 8. I = I + 1
  addi $s0, $s0, 1  # I++

  # step 9. I == 32 ? : end, goto 2
  beq $s0, 32, end_mul_unsigned  # (I==32)?:end,contiue;
  j for_loop_mul

  #--------------
end_mul_unsigned:
  move $v0, $s2 # $v0 : Lo part of result = s2 = L 
  move $v1, $s1 # $v1 : Hi part of result = s1 = H
  #restore RTE
  lw  $fp, 52($sp)
  lw  $ra, 48($sp)
  lw  $a0, 44($sp)
  lw  $a1, 40($sp)
  lw  $s0, 36($sp)
  lw  $s1, 32($sp)
  lw  $s2, 28($sp)
  lw  $s3, 24($sp)
  lw  $s4, 20($sp)
  lw  $s5, 16($sp)
  lw  $s6, 12($sp)
  lw  $s7,  8($sp)
  addi $sp, $sp, 52
  jr $ra

#####################################################################
# Procedure 'mul_signed'
# Argument:
#   $a0 : Multiplicand
#   $a1 : multiplier
# Return:
#   $v0 : Lo part of result
#   $v1 : Hi part of result
# Steps:
#   1.  N1 = $a0, N2 = $a1                                       
#   2.  Make N1 two's complement if negative
#   3.  Make N2 two's complement if negative
#   4.  Call unsigned multiplication using N1, N2. 
#   5.  Say the result is Rhi, Rlo
#   6.  Extract $a0[31] and $a1[31] bits and xor between them. 
#   7.  The result of xor $a0[31] and $a1[31] is S.
#   8.  If S is 1, use the 'twos_complement_64bit' to determine 
#       2's complement form of 64 bit number in Rhi, Rlo.
#####################################################################
mul_signed:
  #store RTE - (2 + 10) * 4 = 48 bytes
  addi  $sp, $sp, -52
  sw  $fp, 52($sp)
  sw  $ra, 48($sp)
  sw  $a0, 44($sp) # $a0 : N1
  sw  $a1, 40($sp) # $a1 : N2
  sw  $s0, 36($sp) # N1 = a0 (reserved after mul_unsigned)
  sw  $s1, 32($sp) # N2 = a1 (reserved after mul_unsigned)
  sw  $s2, 28($sp) # 2's complement of N1 if N1 < 0 
  sw  $s3, 24($sp) # 2's complement of N2 if N2 < 0 
  sw  $s4, 20($sp) # Rlo of mul_unsigned
  sw  $s5, 16($sp) # Rhi of mul_unsigned
  sw  $s6, 12($sp) # s6 = a0[31]
  sw  $s7,  8($sp) # s7 = a1[31]
  addi  $fp, $sp, 52
  #--------------
  # step 1. N1 = $a0, N2 = $a1                                       
  move $s0, $a0 # s0 = N1
  move $s1, $a1 # s1 = N2

  # step 2. Make N1 two's complement if negative
  move $a0, $s0 # a0 = N1 to use twos_complement_if_neg
  jal twos_complement_if_neg  # v0 : 2's complement of a0
  move $s2, $v0 # s2 : 2's complement of N1

  # step 3. Make N2 two's complement if negative
  move $a0, $s1 # a0 = N2 to use twos_complement_if_neg
  jal twos_complement_if_neg  # v0 : 2's complement of a0
  move $s3, $v0 # s3 : 2's complement of N2

  # step 4. Call unsigned multiplication using N1, N2. 
  move $a0, $s2 # a0 = s2 = N1
  move $a1, $s3 # a1 = s3 = N2
  jal mul_unsigned  # v0: Lo, v1: Hi

  # step 5. Say the result is Rhi, Rlo
  move $s4, $v0 # Rlo of mul_unsigned
  move $s5, $v1 # Rhi of mul_unsigned

  # step 6. Extract $a0[31] and $a1[31] bits and xor between them. 
  li $t5, 31
  extract_nth_bit($s6, $s0, $t5)  # s6 = a0[31], where a0 = s0
  extract_nth_bit($s7, $s1, $t5)  # s7 = a1[31], where a1 = s1

  # step 7. The result of xor $a0[31] and $a1[31] is S.
  xor $t6, $s6, $s7

  # step 8. If S is 1, use the 'twos_complement_64bit' to determine 
  #         2's complement form of 64 bit number in Rhi, Rlo.
  # if S != 1, goto end
  bne $t6, 1, end_mul_signed 
  # else S == 1, convert to 2's complement of Rlo, Rhi
  move $a0, $s4 # $a0 : Lo of the number
  move $a1, $s5 # $a1 : Hi of the number
  jal twos_complement_64bit # 
  # $v0 : Lo part of 2's complemented 64 bit
  # $v1 : Hi part of 2's complemented 64 bit

  #--------------
end_mul_signed:
  #restore RTE
  lw  $fp, 52($sp)
  lw  $ra, 48($sp)
  lw  $a0, 44($sp)
  lw  $a1, 40($sp)
  lw  $s0, 36($sp)
  lw  $s1, 32($sp)
  lw  $s2, 28($sp)
  lw  $s3, 24($sp)
  lw  $s4, 20($sp)
  lw  $s5, 16($sp)
  lw  $s6, 12($sp)
  lw  $s7,  8($sp)
  addi $sp, $sp, 52
  jr $ra

#####################################################################
#######  DIVISION ###################################################
#####################################################################
# Procedure 'div_unsigned'
# Argument:
#   $a0 : Dividend
#   $a1 : Divisor
# Return:
#   $v0 : Quotient
#   $v1 : Remainder
# Steps:
#   1. I = 0; Q = DVND; D = DVSR; R = 0
#   2. R = R << 1 
#   3. R[0] = Q[31]
#   4. Q = Q << 1 
#   5. S = R - D
#   6. S < 0 ? goto-9 (YES): goto-7 (NO) 
#   7. R = S
#   8. Q[0] = 1
#   9. I += 1
#  10. I == 32 ? END : goto-2
#####################################################################
div_unsigned:
  #store RTE - (2 + 8) * 4 = 40 bytes
  addi  $sp, $sp, -44
  sw  $fp, 44($sp)
  sw  $ra, 40($sp)
  sw  $a0, 36($sp) # $a0 : Dividend
  sw  $a1, 32($sp) # $a1 : Divisor
  sw  $s0, 28($sp) # I
  sw  $s1, 24($sp) # Q
  sw  $s2, 20($sp) # D
  sw  $s3, 16($sp) # R
  sw  $s4, 12($sp) # Q31  
  sw  $s5,  8($sp) # S  
  addi  $fp, $sp, 44
  #--------------

  # step 1. I = 0; Q = DVND; D = DVSR; R = 0
  li $s0, 0     # s0 = I
  move $s1, $a0 # s1 = Q = Dividend
  move $s2, $a1 # s2 = D = Divisor
  li $s3, 0     # s3 = R = Remainder
  
for_loop_div:  
  # step 2. R = R << 1 
  sll $s3, $s3, 1  # left shift

  # step 3. R[0] = Q[31]
  li $t4, 31
  extract_nth_bit($s4, $s1, $t4)  # s4 = Q[31]
  #insert_to_nth_bit($regDestination, $regBitPosition, $regSourceBit, $regTempMask)
  insert_to_nth_bit($s3, $zero, $s4, $t9) # s3 = R[0] = Q[31]

  # step 4. Q = Q << 1 
  sll $s1, $s1, 1  # left shift

  # step 5. S = R - D
  move $a0, $s3   # a0 = R; first # 
  move $a1, $s2   # a1 = D; second #
  jal sub_logical # S = R - D
  move $s5, $v0   # s5 = S
  
  # 6. S < 0 ? goto-9 (YES, negative): goto-7 (NO, positive) 
  blt $s5, $zero, nextIndex # S is negative

  # step 7. R = S
  move $s3, $s5

  # step 8. Q[0] = 1
  li $t5, 1
  insert_to_nth_bit($s1, $zero, $t5, $t9) # s1 = Q[0] = 1
 
  # step 9. I += 1
nextIndex:
  addi $s0, $s0, 1  # I += 1

  # step 10. I == 32 ? END : goto-2
  beq $s0, 32, end_div_unsigned  # (I==32)?:end,contiue;
  jal for_loop_div

  #--------------
end_div_unsigned:
  move $v0, $s1 #   $v0 : Quotient  # s1 = Q = Dividend
  move $v1, $s3 #   $v1 : Remainder # s3 = R = Remainder
  #restore RTE
  lw  $fp, 44($sp)
  lw  $ra, 40($sp)
  lw  $a0, 36($sp)
  lw  $a1, 32($sp)
  lw  $s0, 28($sp)
  lw  $s1, 24($sp)
  lw  $s2, 20($sp)
  lw  $s3, 16($sp)
  lw  $s4, 12($sp)
  lw  $s5,  8($sp)
  addi $sp, $sp, 44
  jr $ra

#####################################################################
# Procedure 'div_signed'
# Argument:
#   $a0 : Dividend
#   $a1 : Divisor
# Return:
#   $v0 : Quotient
#   $v1 : Remainder
# Steps:
#   1. N1=$a0,N2=$a1
#   2. Make N1 two's complement if negative
#   3. Make N2 two's complement if negative
#   4. Call unsigned Division using N1, N2. 
#   5. Say the result is Q and R
#   6. Determine sign S of Q
#  6A. Extract $a0[31] and $a1[31] bits and xor between them
#  6B. The xor result is S.
#  6C. If S is 1, use the 'twos_complement' for 2's complement of Q
#   7. Determine sign S of R
#  7A. Extract $a0[31] and assign this to S
#  7B. If S is 1, use the 'twos_complement' for 2's complement of R
#####################################################################
div_signed:
  #store RTE - (2 + 10) * 4 = 48 bytes
  addi  $sp, $sp, -52
  sw  $fp, 52($sp)
  sw  $ra, 48($sp)
  sw  $a0, 44($sp) # $a0 : Dividend
  sw  $a1, 40($sp) # $a1 : Divisor
  sw  $s0, 36($sp) # N1 = a0
  sw  $s1, 32($sp) # N2 = a1
  sw  $s2, 28($sp) # 2's complement of N1 if N1 < 0 
  sw  $s3, 24($sp) # 2's complement of N2 if N2 < 0 
  sw  $s4, 20($sp) # Q
  sw  $s5, 16($sp) # R
  sw  $s6, 12($sp) # a0[31]
  sw  $s7,  8($sp) # a1[31]
  addi  $fp, $sp, 52
  #--------------

  # step 1. N1 = $a0, N2 = $a1
  move $s0, $a0 # s0 = N1 = a0
  move $s1, $a1 # s1 = N2 = a1
  
  # step 2. Make N1 two's complement if negative
  move $a0, $s0 # a0 = N1 to use twos_complement_if_neg
  jal twos_complement_if_neg  # v0 : 2's complement of a0
  move $s2, $v0 # s2 : 2's complement of N1
  
  # step 3. Make N2 two's complement if negative
  move $a0, $s1 # a0 = N2 to use twos_complement_if_neg
  jal twos_complement_if_neg  # v0 : 2's complement of a0
  move $s3, $v0 # s3 : 2's complement of N2

  # step 4. Call unsigned Division using N1, N2. 
  move $a0, $s2 # a0 = s2 = N1
  move $a1, $s3 # a1 = s3 = N2
  jal div_unsigned  # result in v0 : Quotient; v1 : Remainder

  # step 5. Say the result is Q and R
  move $s4, $v0  # s4 = v0 : Quotient
  move $s5, $v1  # s5 = v1 : Remainder
  
  # step 6. Determine sign S of Q
  # step 6A. Extract $a0[31] and $a1[31] bits and xor between them
  li $t5, 31
  extract_nth_bit($s6, $s0, $t5)  # s6 = a0[31], where a0 = s0
  extract_nth_bit($s7, $s1, $t5)  # s7 = a1[31], where a1 = s1

  # step 6B. The xor result is S.
  xor $t6, $s6, $s7

  # step 6C. If S is 1, use the 'twos_complement' for 2's complement of Q
  bne $t6, 1, nextR # is S != 1 (i.e. negative), convert to 2's complement of Q
  move $a0, $s4 # s4 = Quotient
  jal twos_complement_64bit 
  move $s4, $v0 # s4 = 2's complement form of Quotient

nextR:
  # step 7. Determine sign S of R
  # step 7A. Extract $a0[31] and assign this to S
  li $t5, 31
  extract_nth_bit($s6, $s0, $t5)  # s6 = a0[31], where a0 = s0
  insert_to_nth_bit($t7, $zero, $s6, $t9) # (t7 = S) = (s6 = a0[31])

  # step 7B. If S is 1, use the 'twos_complement' for 2's complement of R
  bne $t7, 1, end_div_signed # is S != 1 (i.e. negative), convert to 2's complement of Q
  move $a0, $s5 # s5  = Remainder
  jal twos_complement_64bit 
  move $s5, $v0 # s5 = 2's complement form of Remainder

  #--------------
#   $v0 : Quotient
#   $v1 : Remainder
end_div_signed:
  move $v0, $s4 # s4 = Q
  move $v1, $s5 # s5 = R
  #restore RTE
  lw  $fp, 52($sp)
  lw  $ra, 48($sp)
  lw  $a0, 44($sp)
  lw  $a1, 40($sp)
  lw  $s0, 36($sp)
  lw  $s1, 32($sp)
  lw  $s2, 28($sp)
  lw  $s3, 24($sp)
  lw  $s4, 20($sp)
  lw  $s5, 16($sp)
  lw  $s6, 12($sp)
  lw  $s7,  8($sp)
  addi $sp, $sp, 52
  jr $ra

