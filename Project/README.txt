CS47-ProjectI
=============

Instruction:
1. Download CS47ProjectI.zip and unzip it.

2. Assemble proj­auto­test.asm and execute.  Make sure to turn on 
'Assembles all files in directory' and 
'Initialize program counter to global main if defined' option in MARS tool. 
It should generate the following output:
Total passed 0 / 40
*** OVERALL RESULT FAILED ***

3.  Complete the following two procedures in 
    CS47_proj_alu_normal.asm and 
    CS47_proj_alu_logical.asm. 
You may include your required macros (those you write) in the cs47_proj_macro.asm. 
Do not update proj­auto­test.asm or cs47_proj_procs.asm or cs47_common_macro.asm.

4. au_normal (in CS47_proj_alu_normal.asm): 
It takes three arguments as 
  $a0 (First operand), 
  $a1 (Second operand), 
  $a2 (Operation code '+', '­', '*', '/' ­ ASCII code). 
It returns result in $v0 and $v1 
(for multiplication $v1 it will contain HI, for division $v1 will contain remainder). 
This procedure uses normal math operations of MIPS to compute the result (add, sub, mul and div).

5. au_logical (in CS47_proj_alu_logical.asm): 
It takes three arguments as 
  $a0 (First operand), 
  $a1 (Second operand), 
  $a2 (Operation code '+', '­', '*', '/' ­ ASCII code). 
It returns result in $v0 and $v1
(for multiplication $v1 it will contain HI, for division $v1 will contain remainder). 
The evaluation of mathematical operations should use MIPS logic operations only 
(result should not be generated directly using MIPS mathematical operations). 
The implementation needs to follow the digital algorithm implemented in hardware 
to implement the mathematical operations.

6. Assemble proj­auto­test.asm and execute. 
It should generate the following output.
Total passed 40 / 40
*** OVERALL RESULT PASSED ***

NB: Please check each operation result manually from output. 
Your implementation of normal and logical ALU will be compared against
a reference normal and logical ALU result. 
Therefore it is possible that you may see 40/40 of pass, 
but in our test it will not be so.

7. Write a project report including the following sections. 
It is recommended to write report in IEEE format 
[Template and Instructions on How to Create Your Paper 
(https://www.ieee.org/conferences/publishing/templates.html) ]. 
A report written in IEEE format will earn 10% extra credit 
on top of overall project credit. 

8. The final report uploaded: 
* Include clear diagrams for requirement and design 
* Include code snippet to explain implementation 
* Include screen shots of testing results
* Introduction containing objective.
* Requirement
* Design and Implementation Testing
* Conclusion

9. Make sure to upload two files in Canvas 
(No evaluation will be done if both of them are not uploaded) 
* cs47_proj_report.pdf : Project report
* cs47_proj_source.zip: including
  CS47_proj_alu_normal.asm, 
  CS47_proj_alu_logical.asm and 
  cs47_proj_macro.asm. 
No need to upload other files.
