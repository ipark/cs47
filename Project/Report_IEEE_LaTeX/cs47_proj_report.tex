\documentclass[conference]{IEEEtran}
\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnote. 
% If that is unneeded, please comment it out.
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\usepackage{listings} % needed for the inclusion of source code
\usepackage{mips}
\usepackage[capitalise]{cleveref}
%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ASSEMBLY SYNTAX %%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{ %
	language=[mips]Assembler,      
  basicstyle=\footnotesize\ttfamily,
	numbers=left,      
	frame=tb,            
  captionpos=t,
	numberstyle=\tiny\ttfamily\color{gray}, 
  float=tp,
  floatplacement=tbp,
	stepnumber=1,                  
	numbersep=5pt,                  
	backgroundcolor=\color{white},  
	showspaces=false,               
	showstringspaces=false,         
	showtabs=false,                 
	rulecolor=\color{black},        
	tabsize=3,                      
	breaklines=true,      
	breakatwhitespace=false,        
	keywordstyle=\color{blue},     
	commentstyle=\color{dkgreen},  
	stringstyle=\color{mauve},     
	escapeinside={\%*}{*)},        
	morekeywords={*,...}           
}
\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}

\renewcommand{\ttdefault}{cmtt}
\renewcommand\lstlistingname{\textsc{Code Snippet}}
\crefname{listing}{\textsc{Code Snippet}}{\textsc{Code Snippet}}


%%%%%%%%%%%%%%%%%%%%%%%%%%
%% BEGIN DOCUMENT %%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TITLE %%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{Arithmetic Logical Unit (ALU) Calculator : Design and Implementation in MIPS}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% AUTHOR %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\author{\IEEEauthorblockN{Inhee Park}
\IEEEauthorblockA{\textit{Department of Computer Science}, 
\textit{San Jose State University}, San Jose, CA, USA \\ inhee.park@sjsu.edu} }

\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ABSTRACT %%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{abstract}
The objective of this report is to show how to design and implement logical calculator 
in MIPS assembly language on the virtual MIPS environment using MARS simulator. 
While MIPS provides normal instructions for the arithmetic operations (+, -, *, and /)
we would build the same arithmetic operations in assembly at the interface with hardware 
level based on the combinational logic circuit design consisting of logic gates, 
such as AND, OR, NOT and XOR. 
\end{abstract}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% KEY WORDS %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{IEEEkeywords}
Arithmetic Logical Unit (ALU), MIPS, MARS, bit shift, Runtime Stack Frame.
\end{IEEEkeywords}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INTRODUCTION %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
Arithmetic Logic Unit (ALU) is a combinational digital circuit 
performing arithmetic and bit-wise operations on integer binary numbers,
where the Boolean Algebra plays the fundamental operations\cite{L13}.

MIPS\footnote{Microprocessor without Interlocked Pipelined Stages}
is a processor architecture for Reduced Instruction Set Computer (RISC) 
Instruction Set Architecture (ISA).  Such a reduced instruction is
achieved by gate level minimization utilizing K-map optimization\cite{L14}.

In this report, we use MIPS Assembly language\cite{Textbook} to build the ALU calculator
by considering binary operation of integer operands.  


%%%%%%%%%%%%%%%%%%%%%%%%%%
%% REQUIREMENT %%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Requirements of Arithmetic Procedures}
\begin{itemize}
\item 
  Download \texttt{CS47ProjectI.zip} and unzip it.
\item 
Turn on the {\it 'Assembles all files in directory'} and 
{\it 'Initialize program counter to global main if defined'} option in MARS tool. 
Assemble \texttt{proj-auto-test.asm} and execute, which should generate the following output:
\begin{verbatim}
Total passed 0 / 40
*** OVERALL RESULT FAILED ***
\end{verbatim}
\item 
  Complete the following two procedures in 
    \texttt{CS47\_proj\_alu\_normal.asm} and \texttt{CS47\_proj\_alu\_logical.asm},
    also including macros \texttt{cs47\_proj\_procs.asm} and \texttt{cs47\_common\_macro.asm}.
\item 
{\bf au\_normal : } The \texttt{CS47\_proj\_alu\_normal.asm}
takes three arguments as 
\$a0 (First operand), 
\$a1 (Second operand), 
\$a2 (Operation code '+', '­', '*', '/' ­ ASCII code) and returns result in \$v0 and \$v1 
(for multiplication \$v1 it will contain HI, for division \$v1 will contain remainder). 
This procedure uses normal math operations of MIPS to compute the result (add, sub, mul and div).

\item
{\bf au\_logical : } The \texttt{CS47\_proj\_alu\_logical.asm}) 
takes three arguments as 
\$a0 (First operand), 
\$a1 (Second operand), 
\$a2 (Operation code '+', '­', '*', '/' ­ ASCII code), and returns result in \$v0 and \$v1.
(for multiplication \$v1 it will contain HI, for division \$v1 will contain remainder). 
The evaluation of mathematical operations should use MIPS logic operations only 
(result should not be generated directly using MIPS mathematical operations). 
The implementation needs to follow the digital algorithm implemented in hardware 
to implement the mathematical operations.

\item 
After implementation of \texttt{CS47\_proj\_alu\_normal.asm} and \texttt{CS47\_proj\_alu\_logical.asm}, assemble and execute\texttt{proj-­auto-test.asm} should generate the following output:
\begin{verbatim}
Total passed 40 / 40
*** OVERALL RESULT PASSED ***
\end{verbatim}
\end{itemize}


\section{Simulator Installation and Setup}
SPIM and MARS\footnote{MIPS Assembler and Runtime Simulator}
are the MIPS simulators to provide virtual computing environment
for the MIPS processor to run assembly language code.\cite{L5} 
In our CS47 class, we use MARS as an IDE\footnote{Integrated Development Environment},
which has features such as register window, text segment window, data segment window
and console interface. The latests version of MARS 4.5 (August 2014) was downloaded from 
\texttt{http://courses.missouristate.edu/KenVollmar/mars/} and installed on the
MacBook Pro (Retina, 13-inch, Late 2012) with specification of
Professor (2.9 GHz Intel Core i7), Memory (8 GB 1600 MHz DDR3) and OS
(macOS Sierra version 10.12.6 (16G1510)). Although the MARS program is distributed
as a JAR (Java ARchive) format, which aggregates many Java class files and 
associated metadata and resources into one file for distribution, thus it is platform independent.

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% PROCEDURE %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Overall Design and Implementation of Procedures}
Each procedure allots a specific stack frame size depending on the
number of registers used during the operation of the procedure. 
The stack frame size allocation is important because
it forms a contract between the caller and the callee
that defines how arguments are passed between them, how the result of a procedure is passed  from the callee to the caller, and how registers are shared.
The procedure should push its stack frame onto the stack on entry into the 
procedure by subtracting the size of its stack frame from the stack pointer, \$sp. 
Before exit from the procedure by returning the address \$ra and output \$v0-\$v1, 
we should restore the stack point back to original.
\cite{L7,L8,L9}


\subsection{Normal Arithmetic Operations}
\cref{code:normal} displays the MIPS code snippet. Basically using the
MIPS instruction, branching out the addition, subtraction, multiplication, and division
procedures depending on the ASCII code from the argument stored in register \$a2.
\lstinputlisting[caption={au\_normal:},label={code:normal}]{Code/normal.asm}

\subsection{Logical Arithmetic Operations}
Overall logical arithmetic implementations are built up with independent procedure calls by each operation and by common functionality.  Such procedure calls are summarized in the 
\cref{table:summary}, which shows stack frame size for input and output registers along with other data registers as well as corresponding callees.
\lstinputlisting{Code/au-logical.asm}
\begin{table*}[h!]
\caption{Summary of Procedure Calls for ALU Logical Implementation}
\label{table:summary}
\begin{tabular}{l|l|l|l|l|l|l|l}
\hline 
Procedure               & Stack  & Input               & Extra     & \multicolumn{2}{c|}{Output}      & Callee & Macros\\                   
(Caller)                & Frame  & Registers           & Saved     & \multicolumn{2}{c|}{Registers}  &        &       \\
                        & (bits) &                     & Registers & \$v0          & \$v1    &  &           \\\hline\hline

au\_logical:            & 24       & \$a0: 1st\#         &           & \$a0+\$a1     &         & &  \\
                        &          & \$a1: 2nd\#         &           & \$a0-\$a1     &         & & \\
                        &          & \$a2: op.mode       &           & \$a0*\$a1:Lo  & \$a0*\$a1:Hi & & \\ 
                        &          & ('+','-','*','/')   &           & \$a0/\$a1(Q)  & \$a0\%\$a1(R) & & \\\hline\hline

add\_sub\_logical:      & 56       & \$a0: 1st\#         & \$s0-\$s7   & \$a0+\$a1    & Final carryout  &  & extract\_nth\_bit\\                   
                        &          & \$a1: 2nd\#         &           & \$a0-\$a1    &                     & &  insert\_to\_nth\_bit\\
                        &          & \$a2: 0x0(+)        &           &              & &  & \\
                        &          &  0xFFFFFFFF(-)      &           &              & &  & \\\hline

add\_logical:           & 24       & \$a0: 1st \#        &           & \$a0+\$a1    & & add\_sub\_logical & \\
                        &          & \$a1: 2nd \#        &           &              & & & \\
                        &          & \$a2: 0x00000000    &           &              & & & \\\hline

sub\_logical:           & 24       & \$a0: 1st \#        &           &              & & add\_sub\_logical & \\
                        &          & \$a1: 2nd \#        &           &              & &  & \\
                        &          & \$a2: 0xFFFFFFFF    &           &              & & & \\\hline\hline

mul\_unsigned:          & 48       & \$a0: Multiplicand  & \$s0-\$s7 &\$a0*\$a1:Lo  & \$a0*\$a1:Hi & bit\_replicator  & extract\_nth\_bit \\
                        &          & \$a1: Multiplier    &           &              & & add\_logical& insert\_to\_nth\_bit\\
                        &          &                     &           &              & &  & \\
                        &          &                     &           &              & &  & \\\hline

mul\_signed:            & 48       & \$a0: Multiplicand  & \$s0-\$s7 &\$a0*\$a1:Lo  & \$a0*\$a1:Hi   & twos\_complement\_if\_neg & \\
                        &          & \$a1: Multiplier    &           &              & & mul\_unsigned & extract\_nth\_bit \\                        
                        &          &                     &           &              & & twos\_complement\_64bit & \\\hline\hline

div\_unsigned:          & 40       & \$a0: Dividend      & \$s0-\$s5   & \$a0/\$a1(Q)  & \$a0\%\$a1(R)  & sub\_logical & extract\_nth\_bit \\
                        &          & \$a1: Divisor       &           &              & & & insert\_to\_nth\_bit\\\hline

div\_signed:            & 48       &  \$a0: Dividend     & \$s0-\$s7   & \$a0/\$a1(Q)  & \$a0\%\$a1(R) & twos\_complement\_if\_neg & extract\_nth\_bit \\
                        &          &  \$a1: Divisor      &           &              & & div\_unsigned & insert\_to\_nth\_bit\\
                        &          &                     &           &              & & twos\_complement\_64bit & \\\hline\hline

twos\_complement:       & 16       & \$a0: number        &           &              & & add\_logical & \\\hline

twos\_complement\_if\_neg: & 16       & \$a0: number        &           &              & & twos\_complement & \\\hline

twos\_complement\_64bit:  & 32       & \$a0: Lo part of \# &           & Lo part of   & Hi part of   & add\_logical & insert\_to\_nth\_bit\\
                          &          & \$a1: Hi part of \# &           & 2's comple.  & 2's comple.  & & \\\hline 

bit\_replicator:          & 16       & \$a0: 0x0  & & 0x00000000 & & &  \\
                          &            & \$a0: 0x1  & & 0xFFFFFFFF  & &  & \\\hline\hline
\end{tabular}                                                              
\end{table*} 
%.globl au\_logical


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% COMMON PROCEDURE/MACROS  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Common Procedures and Macros}
When a small number of code is repetitively used by multiple procedures,
it is convenient practice to make them as a macro. We made two frequently
used functionalities, namely, extraction of a specific bit from a given
bit pattern as well as insertion of a given bit to a target bit pattern.

However, when such a repetitively used chunk of code is longer, it would
be better to make it a procedure by allocating a stack frame size.

\subsection{Macro `extract\_nth\_bit'}
As we are dealing with bit-wise operation for binary numbers,
it is useful to make a macro extracting a specific bit from the bit pattern.
To do so, firstly right shift the source bit pattern by number of bits specified by the requested
bit position by using \texttt{srlv} instruction. 
By AND operation between the mask from previous step with \texttt{0x1}, we can extract the bit
at the requested position.
\lstinputlisting[caption={extract\_nth\_bit:},label={code:extract-nbit}]{Code/extract.asm}

\subsection{Macro `insert\_to\_nth\_bit'}
Firstly, prepare the mask by left shift of \texttt{0x1} by amount of specified bit position and inverting it,
so that we can mask other bits except the bit position that we want to insert the bit.
Secondly, shift left the source bit by amount of the specified bit position. 
Then logically OR this pattern with the destination register will result insertion to the specified bit position.
\lstinputlisting[caption={insert\_to\_nth\_bit:},label={code:insert-nbit}]{Code/insert.asm}


\subsection{Procedure `bit\_replicator'}
As the registers in MIPS is 32-bit, as we load immediate either 0 or -1, we can replicate
the bit upto 32-bit.
\lstinputlisting[caption={bit\_replicator},label={code:bit-replicator}]{Code/bit-replicator.asm}

\subsection{Procedure `twos\_complement'}
By definition, 2's complement is by inverting the number and add 1 bit \cite{L3}.
We can do so by using \texttt{not} instruction to invert the bit.
\lstinputlisting[caption={twos\_complement},label={code:2scomplement}]{Code/twos-complement.asm}

\subsection{Procedure 'twos\_complement\_if\_neg'}
Determine the sign of the input bit and if it's negative, call the procedure \texttt{twos\_complement}. 
Otherwise pass the input to the \$v0 as it is.
\lstinputlisting[caption={twos\_complement\_if\_neg},label={code:2scomplementifneg}]{Code/twos-complement-if-neg.asm}

\subsection{Procedure `twos\_complement\_64bit'}
Firstly convert the content of the first 32-bit register, which stores LO part, to 2's complement
(~\$a0 + 1).
Then convert the second 32-bit register, which stores HI part, 
and logical addition with the carry-out from the previous logical addition.
\lstinputlisting[caption={twos\_complement\_64bit},label={code:2scomplement64bit}]{Code/twos-complement-64bit.asm}

  
%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ADD/SUB %%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Logical Addition/Subtraction}
Logic gate circuit is implemented by boolean function, to mimic how this
circuit works, we must be clear how the bit-wise binary operations are carried out \cite{L13,L14}.
That is directly useful to write corresponding MIPS assembly instructions.
In logical addition procedure, each MIPS instruction basically
follows the bit-wise addition depicted in \cref{f:binaryAdd}.
\subsection{Binary Addition} 
\begin{figure}[h!]\
\includegraphics[width=0.5\textwidth]{./Figs/bitAddition.png}
\caption{Binary Bit-wise addition scheme}
  \label{f:binaryAdd}
\end{figure}

\subsection{Design \& Diagrams for Addition/Subtraction}
In general, the first step to design the ALU, the combinational circuit, is
to identify specification of the target circuit functionality. \cite{L16}
The second step is to construct the truth table of the logic function 
based on the digital circuit requirement (aka specification, which min-terms are 1).
The Boolean logic function is then reduced by optimizing Boolean algebra and K-Map\cite{L13,L14}.
In particular for the logical addition/subtraction, corresponding truth tables based
on the combinational circuit functionalities are depicted in \cref{f:halfAdd} for half adder 
in \cref{f:fullAdd} for full adder.  
\begin{figure}[h!]
	\includegraphics[width=0.5\textwidth]{./Figs/TThalfAdder.png}
	\caption{Truth table and logic circuit for half adder}
	\label{f:halfAdd}
\end{figure}

\begin{figure}[h!]
	\includegraphics[width=0.5\textwidth]{./Figs/fullAdderTTKmap.png}
	\caption{Truth table of full adder (A) and K-map (B). 
		Implementation of full adder with two half adders and an OR gate}
	\label{f:fullAdd}
\end{figure}
The next step is Technology Mapping, where Boolean functions are expressed 
in terms of corresponding Boolean operations shown in the combinational adder/subtracter
circuit \cref{f:addSubCombo}.
\begin{figure}[h!]
\includegraphics[width=0.5\textwidth]{./Figs/rippleAddSubComb.png}
  \caption{Simplified Combined Adder/Subtracter by XOR gate}
  \label{f:addSubCombo}
\end{figure}


\subsection{Code Snippet for Logical Addition}

Given the bit-wise binary addition scheme (\cref{f:binaryAdd}) and 
the logic circuit (\cref{f:addSubCombo}), we can construct a workflow to be implemented 
by MIPS instruction line-by-line depicted in \cref{f:addFlow}. 
\begin{figure}[h!]
	\includegraphics[width=0.5\textwidth]{./Figs/addSubFlow.png}
	\caption{Flowchart for add\_sub\_logical procedure}
	\label{f:addFlow}
\end{figure}
The MIPS code for the common procedure for addition and subtraction is displayed in 
\cref{code:commonAddSub}.
From the LSB\footnote{Lowest Significant Bit} of a number, 
extract the first bit, and calculate sum bit (Y) and carry bit (CI and CO) bit 
following the full-adder function as described in \cref{f:fullAdd}.
Then insert the sum bit (Y) to the return register \$v0, and iterate these
operations until ending bit of the number (32-bit). 
Final carry-out bit should return to \$v1 register.
\lstinputlisting[caption={add\_sub\_logical},label={code:commonAddSub}]{Code/add-sub-logical.asm}

\lstinputlisting[caption={add\_logical}]{Code/add-logical.asm}
This procedure stores two operands in register \$a0 and \$a1, and
set \$a2 register to store 0x00000000 bit pattern indicating
addition should be performed in the procedure \texttt{add\_sub\_logical}.

\subsection{Code Snippet for Logical Subtraction}
This procedure stores two operands in register \$a0 and \$a1, and
set \$a2 register to store 0xFFFFFFFF bit pattern indicating
subtraction should be performed in the procedure \texttt{add\_sub\_logical}.

\lstinputlisting[caption={sub\_logical}]{Code/sub-logical.asm}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% MULTIPLICATION %%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Logical Multiplication}

\subsection{Binary Addition} 
First operand is a multiplicand; second operand is a multiplier.
If LSB of multiplier is 1, multiplicand is added to the product.
At each step, this partial product is shifted left by one bit 
to add zero padding to the multiplicand. Also, multiplier is
shifted right one bit.  At the end of the procedure (reaching
to the MSB of the multiplier), all these partial products are summed up 
to get the final multiplication result.
\begin{figure}[h!]
\includegraphics[width=0.5\textwidth]{./Figs/mulFlow.png}
\caption{Flowchart for multiplication procedure}
  \label{f:flowchart-mul}
\end{figure}

\subsection{Design \& Diagrams for Multiplication}
Instead of bit shift for both multiplicand and multiplier shown in 
\cref{f:flowchart-mul}, 
the use of fixed 32-bit register for the multiplicand simplifies
the multiplication procedure as depicted in \cref{f:flowchart-replicator}.
To achieve this strategy, LSB of multiplier is replicated upto 32-bit.
(\cref{code:bit-replicator}). 
\begin{figure}[h!]
  \includegraphics[width=0.4\textwidth]{./Figs/mulFlowReplicator.png}
\caption{Flowchart for multiplication procedure}
  \label{f:flowchart-replicator}
\end{figure}


\begin{figure}[h!]
\includegraphics[width=0.5\textwidth]{./Figs/mul_unsigned_logic.png}
\caption{Logic of unsigned multiplication}
\end{figure}


\begin{figure}[h!]
\includegraphics[width=0.5\textwidth]{./Figs/mul_signed_logic.png}
\caption{Logic of signed multiplication}
\end{figure}

\subsection{Code Snippet for Unsigned Multiplication}
Extract the first bit of the multiplier and make it 32-bit repetition
using the procedure \texttt{bit\_replicator} in \cref{code:bit-replicator}.
Next, this 32-bit repeated multiplier (R) is AND operation 
with the multiplicand (M) stored in the fixed 32-bit register. ($X = M \& R$)
Multiplication of two 32-bit operands will result in 64-bit product.
Thus the product register is 64-bit: upper 32-bit (HI) stores the partial
product ($H = H + X$); lower 32-bit (LO) is shared with the 32-bit multiplier
($L[31]=H[0]$). In order to repeat this procedure until reaching the MSB of multiplier,
we firstly shift right multiplier by one bit at a time ($L = L >> 1$), 
then insert first bit of HI to the last bit of the multiplier ($L[31]=H[0]$), followed by
shift right for the HI ($H = H >> 1$).
\lstinputlisting[caption={mul\_unsigned}]{Code/mul-unsigned.asm}

\subsection{Code Snippet for Signed Multiplication}
Firstly, convert multiplier and multiplicand to its 2's complement
if thye are negative using the common procedure \texttt{tows\_complement\_if\_neg}
shown in \cref{code:2scomplementifneg}.  We can reuse the 
\texttt{mul\_unsinged} procedure regardless of signs of the input numbers.
Due to 64-bit result of the product register by multiplication of 32-bit numbers,
lower and higher part of 32-bit results are saved into \$v0 and \$v1, respectively.
We then XOR operation on the sign bit of each number by extracting its MSB bit.
If this XOR result is 1, indicating the final product is \emph{-ve}, then call the
\texttt{twos\_complement\_64bit} (shown in \cref{code:2scomplement64bit})
to make the final product as 64-bit of 2's complement form.
\lstinputlisting[caption={mul\_signed}]{Code/mul-signed.asm}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% DIVISION %%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Logical Division}
We can process the binary division in the following manner depicted in \cref{f:divFlow} (B):
Firstly, align the divisor (Y) onto the MSB part of the dividend.
Secondly, denote the aligned bits of the dividend X. 
Next, compare X and Y.  If $X \ge Y$, set the quotient bit (Q) 1, carry out logical subtraction X-Y; otherwise, set the quotient bit (Q) 0 and skip the subtraction step. Finally, right shift the divisor (Y) by one bit and repeat the procedure until consumption of all bits in divisor.

In the same manner, as depicted in \cref{f:divFlow} (A), we can construct the workflow for logical division procedure using the 32-bit Divisor register (D) and 64-bit register for shared by remainder (R) and Dividend (Q).   
\subsection{Binary Division}
\begin{figure}[h!]
\includegraphics[width=0.5\textwidth]{./Figs/divFlow.png}
  \caption{Flowchart for division procedure \& Binary Division}
  \label{f:divFlow}
\end{figure}

\subsection{Design \& Diagrams for Division}
\cref{f:unsignedDiv} shows that optimization of the division circuit by employing 
64-bit register shared by Remainder (R) occupying the upper 32-bit and by Quotient 
(Q) by loading dividend at the lower 32-bit. This design is very similar to the 
logical multiplication, where the 64-bit product register is shared by partial 
product at the upper 32-bit (HI) by multiplier at the lower 32-bit (LO). Using 
the fixed 32-bit register for the divisor (D), we don't need a bit shift for the divisor.
Starting with the MSB of the dividend $Q[31]$, which is inserted into Remainder register 
at $R[0]$, then determine the sign of the logical subtraction between dividend (R) and
divisor (D), i.e. $S = R - D$.  If $S$ is positive, insert 1 to the the quotient (Q).
Otherwise, skip the logical subtraction and increase index bit and continue until 32-bit.
\begin{figure}[h!]
	\includegraphics[width=0.5\textwidth]{./Figs/div_unsigned.png}
	\caption{Logic of unsigned division}
	\label{f:unsignedDiv}
\end{figure} 

As depicted in signed bit division circuit in \cref{f:signedDiv},
if sign bits of dividend and divisor are same, the resulting quotient bit is \emph{+ve},
otherwise, \emph{-ve}.  Note that the sign of remainder should be same as the sign of the dividend.
\begin{figure}[h!]
	\includegraphics[width=0.5\textwidth]{./Figs/div_signed.png}
	\caption{Logic of signed multiplication}
	\label{f:signedDiv}
\end{figure}

\subsection{Code Snippet for Unsigned Division}
The logical workflow for the unsigned division described in \cref{f:unsignedDiv}
is correspondingly implemented in MIPS instruction.  The code snippet is displayed in
\cref{code:divUnsigned}.
\lstinputlisting[caption={div\_unsigned},label={code:divUnsigned}]{Code/div-unsigned.asm}


\subsection{Code Snippet for Signed Division}
The procedure of signed division procedure (\cref{divSigned}) is very much similar to that of
signed multiplication (except that store final quotient in \$v0 and
final remainder in \$v1): 
(1) convert divisor and dividend to its 2's complement
if they are negative; (2) reuse the \texttt{div\_unsinged} procedure for the
2's complemented operands; (3) XOR operation on the sign bit of each number by extracting its MSB bit.
If this XOR result is 1, indicating the final product is \emph{-ve}, then call the
\texttt{twos\_complement\_64bit} (shown in \cref{code:2scomplement64bit})
to make the final product as 64-bit of 2's complement form.
\lstinputlisting[caption={div\_signed},label={divSigned}]{Code/div-signed.asm}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% SUMMARY & RESULT %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Testing and Result}
\subsection{Unit Testing}
Although our final goal is to pass all the 40 test cases defined in the 
\texttt{proj-auto-test.asm}, however, testing step is rather unit testing approach. 

That unit testing was possible because we built the final product of the logical 
calculator as an aggregated procedures.  Therefore, it is natural to carry out 
unit testing as suggested in software development procedure.
In our project case, an unit is either a macro or a procedure (with a stack frame 
construction and destruction).

At every completion of each procedure (as well as macro), 
I used single set of operands, closely examined the register window of the MARS,
together with single step execution instead of entire execution.  Especially checking
out register at each instruction execution was crucial practice of testing
whether the set of instruction is behaved as intended. 

For example, while all other procedures are correct, the source of error was originated
incorrectly implemented procedure of \texttt{twos\_complement\_64bit}.
I initially converted both Hi and Lo register to 2's complements and add them, 
which turned out to be an incorrect implementation. 
This experience proved the importance of unit testing in order to avoid
cascade of one incorrect procedure to entangled complication of the entire procedures.

\subsection{Source of Errors}
Based on my implementation procedure, I can think of mainly three cases as a source of error.
(1) mistake in calculating stack frame size;
(2) mistake in register name in instruction command.
Those are the frequently occurred source of error because we used many registered besides
input registers for argument (\$a1-\$a2).

\subsection{Final Result}
This is the screenshot from the MARS, which showed \texttt{Total passed 40 / 40}.
Along with a manual inspection of the testing output in the console interface of the MARS,
both the normal and logical implementations correct by passing the all test cases.
The 40 test cases are constructed by combination of positive and negative operands
along with addition, subtraction, multiplication, and division operator.  On top of that
normal ALU and logical ALU are compared with each other. Therefore, the MIPS implementation
to build the logic calculator is successful.
\begin{figure}[h!]
\includegraphics[width=0.5\textwidth]{./Figs/testResult.png}
\caption{Test restuls}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%
%% CONCLUSION %%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusion}
Initially this project seems to be just pertinent to MIPS assembly language shown in the green sheet.
However, consolidated understanding of the most of lectures including
combinational logic circuits along with its prerequisite for boolean algebra,
truth table, K-map optimization are all necessary to clearly understand and construct
the clear workflow for each arithmetic operation.
Furthermore, rewinding concept to the memory usage (stack frame) and number representation (2's complement)
are all crucial to fully appreciate the underlying logic behind the MIPS instruction implementation.
Overall, this project provides an opportunity to review most of lectures covered in CS47, only equipped with
those fundamentals, implementation in MIPS language was straightforward.  

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% BIBLOIGRAPHY %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{thebibliography}{00}
\bibitem{L3} Kaushik Patra, CS47 Lecture Note 3: Number Representation (Integer Representation, Format conversion, Negative Integer Representation) SJSU, Fall Semester 2018
\bibitem{L5} Kaushik Patra, CS47 Lecture Note 5: Assembler, Linker, Loader (Macros), SJSU, Fall Semester 2018
\bibitem{L6} Kaushik Patra, CS47 Lecture Note 6: SPIM Simulator (Simulator, System Calls, Simple Program, Macro usage), SJSU, Fall Semester 2018
\bibitem{L7} Kaushik Patra, CS47 Lecture Note 7: Memory Usage I (Registers), SJSU, Fall Semester 2018
\bibitem{L8} Kaushik Patra, CS47 Lecture Note 8: Memory Usage II (Memory Model), SJSU, Fall Semester 2018
\bibitem{L9} Kaushik Patra, CS47 Lecture Note 9: Memory Usage III (Memory, Stack Operation, Global/Local Variable), SJSU, Fall 2018
\bibitem{Textbook} David A. Patterson and John L. Hennessy. 2011.  Appendix A. Assemblers, Linkers, and the SPIM Simulator. In Computer Organization and Design: The Hardware/Software Interface (4th ed.) Morgan Kaufmann Publishers Inc., San Francisco, CA, USA.
\bibitem{L12} Kaushik Patra, CS47 Lecture Note 12, Procedures Calls (High/Low Level Procedure Call, Caller Run Time Environment (RTE), RTE Storage), SJSU, Fall Semester 2018
\bibitem{L13} Kaushik Patra, CS47 Lecture Note 13, Boolean Algebra I (Boolean Values, Operations, Functions, Truth Table, Basic Identities \& Algebraic Manipulation), SJSU, Fall Semester 2018
\bibitem{L14} Kaushik Patra, CS47 Lecture Note 14, Boolean Algebra II (Standard Forms, Karnaugh Map), SJSU, Fall Semester 2018
\bibitem{L15} Kaushik Patra, CS47 Lecture Note 15, Logic Gates (Combinational Logic Gates), SJSU, Fall Semester 2018
\bibitem{L16} Kaushik Patra, CS47 Lecture Note 16, Logic Circuit Design (Combinational Circuit Design), SJSU, Fall Semester 2018
\bibitem{L17} Kaushik Patra, CS47 Lecture Note 17, Digital Circuit Components, SJSU, Fall Semester 2018
\bibitem{L18} Kaushik Patra, CS47 Lecture Note 18, Addition-Subtraction Circuit (Binary Addition/Subtraction Operations, Logic Add/Sub in Assembly), SJSU, Fall Semester 2018
\bibitem{L19} Kaushik Patra, CS47 Lecture Note 19, Multiplication (Binary Multiplication Curcuit, Algorithm for Unsigned and Signed Cases), SJSU, Fall Semester 2018
\bibitem{L20}   Kaushik Patra, CS47 Lecture Note 20, Division (Binary Division Curcuit, Algorithm for Unsigned and Signed Cases), SJSU, Fall Semester 2018
\end{thebibliography}
\vspace{12pt}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% END DOCUMENT %%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}

%\ref{AA}\cite{b6}
%\lstinputlisting{../MIPS_Assembly/CS47_proj_alu_logical.asm}
%\begin{lstlisting}
%\end{lstlisting}
