twos_complement_if_neg:
  #store RTE - (2 + 1) * 4 = 12 bytes
  addi  $sp, $sp, -16
  sw  $fp, 16($sp)
  sw  $ra, 12($sp)
  sw  $a0,  8($sp) # first number to be 2's complement
  #--------------
  bgt $a0, $zero, pass # if $a0 > 0, just pass $a0 to $v0
  jal twos_complement
  j end_twos_complement_if_neg
pass:
  move $v0, $a0
  #--------------
end_twos_complement_if_neg:
  #restore RTE
  lw  $fp, 16($sp)
  lw  $ra, 12($sp)
  lw  $a0,  8($sp)
  addi $sp, $sp, 16
  jr $ra
