add_logical:
  #store RTE - (2 + 3) * 4 = 20 bytes
  addi  $sp, $sp, -24
  sw  $fp, 24($sp)
  sw  $ra, 20($sp)
  sw  $a0, 16($sp) # first number
  sw  $a1, 12($sp) # second number
  sw  $a2,  8($sp) # mode
  #--------------
  li $a2, 0 # $a2=0x00000000
  jal add_sub_logical
  j end_add_logical
  #--------------
end_add_logical:
  #restore RTE
  lw  $fp, 24($sp)
  lw  $ra, 20($sp)
  lw  $a0, 16($sp)
  lw  $a1, 12($sp)
  lw  $a2,  8($sp)
  addi  $sp, $sp, 24
  jr $ra
