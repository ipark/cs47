.text
.globl au_logical

au_logical:
  #store RTE - (2 + 3) * 4 = 20 bytes
  addi  $sp, $sp, -24
  sw  $fp, 24($sp)
  sw  $ra, 20($sp)
  sw  $a0, 16($sp) # first number
  sw  $a1, 12($sp) # second number
  sw  $a2,  8($sp) # mode
  #--------------
  beq $a2, '+', add_logical
  beq $a2, '-', sub_logical
  beq $a2, '*', mul_signed
  beq $a2, '/', div_signed
  #--------------
end_au_logical:
  #restore RTE
  lw  $fp, 24($sp)
  lw  $ra, 20($sp)
  lw  $a0, 16($sp)
  lw  $a1, 12($sp)
  lw  $a2,  8($sp)
  addi  $sp, $sp, 24
  jr $ra
