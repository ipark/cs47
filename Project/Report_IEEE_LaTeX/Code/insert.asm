.macro insert_to_nth_bit($regDestination, $regBitPosition, $regSourceBit, $regTempMask)
 li  $regTempMask, 1
 # regTempMask = 1 << regBitPosition
 sllv $regTempMask, $regTempMask, $regBitPosition  
 not  $regTempMask, $regTempMask # invert bit
 # mask regDestination with regTempMask
 and  $regDestination, $regDestination, $regTempMask 
 # regSourceBit = regSourceBit << regBitPosition
 sllv $regSourceBit, $regSourceBit, $regBitPosition 
 # regDestination = regDestination | regSourceBit 
 or   $regDestination, $regDestination, $regSourceBit  
.end_macro

