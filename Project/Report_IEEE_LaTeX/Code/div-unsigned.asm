div_unsigned:
  #store RTE - (2 + 8) * 4 = 40 bytes
  addi  $sp, $sp, -44
  sw  $fp, 44($sp)
  sw  $ra, 40($sp)
  sw  $a0, 36($sp) # $a0 : Dividend
  sw  $a1, 32($sp) # $a1 : Divisor
  sw  $s0, 28($sp) # I
  sw  $s1, 24($sp) # Q
  sw  $s2, 20($sp) # D
  sw  $s3, 16($sp) # R
  sw  $s4, 12($sp) # Q31  
  sw  $s5,  8($sp) # S  
  addi  $fp, $sp, 44
  #--------------

  # step 1. I = 0; Q = DVND; D = DVSR; R = 0
  li $s0, 0     # s0 = I
  move $s1, $a0 # s1 = Q = Dividend
  move $s2, $a1 # s2 = D = Divisor
  li $s3, 0     # s3 = R = Remainder
  
for_loop_div:  
  # step 2. R = R << 1 
  sll $s3, $s3, 1  # left shift

  # step 3. R[0] = Q[31]
  li $t4, 31
  extract_nth_bit($s4, $s1, $t4)  # s4 = Q[31]
  #insert_to_nth_bit($regDestination, $regBitPosition, $regSourceBit, $regTempMask)
  insert_to_nth_bit($s3, $zero, $s4, $t9) # s3 = R[0] = Q[31]

  # step 4. Q = Q << 1 
  sll $s1, $s1, 1  # left shift

  # step 5. S = R - D
  move $a0, $s3   # a0 = R; first # 
  move $a1, $s2   # a1 = D; second #
  jal sub_logical # S = R - D
  move $s5, $v0   # s5 = S
  
  # 6. S < 0 ? goto-9 (YES, negative): goto-7 (NO, positive) 
  blt $s5, $zero, nextIndex # S is negative

  # step 7. R = S
  move $s3, $s5

  # step 8. Q[0] = 1
  li $t5, 1
  insert_to_nth_bit($s1, $zero, $t5, $t9) # s1 = Q[0] = 1
 
  # step 9. I += 1
nextIndex:
  addi $s0, $s0, 1  # I += 1

  # step 10. I == 32 ? END : goto-2
  beq $s0, 32, end_div_unsigned  # (I==32)?:end,contiue;
  jal for_loop_div

  #--------------
end_div_unsigned:
  move $v0, $s1 #   $v0 : Quotient  # s1 = Q = Dividend
  move $v1, $s3 #   $v1 : Remainder # s3 = R = Remainder
  #restore RTE
  lw  $fp, 44($sp)
  lw  $ra, 40($sp)
  lw  $a0, 36($sp)
  lw  $a1, 32($sp)
  lw  $s0, 28($sp)
  lw  $s1, 24($sp)
  lw  $s2, 20($sp)
  lw  $s3, 16($sp)
  lw  $s4, 12($sp)
  lw  $s5,  8($sp)
  addi $sp, $sp, 44
  jr $ra
