mul_unsigned:
  #store RTE - (2 + 10) * 4 = 48 bytes
  addi  $sp, $sp, -52
  sw  $fp, 52($sp)
  sw  $ra, 48($sp)
  sw  $a0, 44($sp) # $a0 : Multiplicand
  sw  $a1, 40($sp) # $a1 : Multiplier
  sw  $s0, 36($sp) # I
  sw  $s1, 32($sp) # H
  sw  $s2, 28($sp) # L=MPLR
  sw  $s3, 24($sp) # M=MCND
  sw  $s4, 20($sp) # R={32{L[0]}}
  sw  $s5, 16($sp) # X=M&R
  sw  $s6, 12($sp) # bit position 31 for L31
  sw  $s7,  8($sp) # H0
  addi  $fp, $sp, 52
  #--------------
  # step 1. I = 0, H = 0, L = MPLR, M = MCND
  li $s0, 0     # s0 = I = 0
  li $s1, 0     # s1 = H = 0
  move $s2, $a1 # s2 = L = MPLR  
  move $s3, $a0 # s3 = M = MCND

for_loop_mul:
  # step 2. R = {32{L[0]}}
  extract_nth_bit($s4, $s2, $zero) # s4 = L[0] = s2[0]
  move $a0, $s4  # to use bit_replicator with $a0 
  jal bit_replicator # output $v0
  move $s4, $v0  # s4 = R = {32{L[0]}}

  # step 3. X = M & R
  and $s5, $s3, $s4  # s5 = X

  # step 4. H = H + X // add_logical
  move $a0, $s1 # H, first number
  move $a1, $s5 # X, second number
  jal add_logical
  move $s1, $v0 # s1 = H
  
  # step 5. L = L >> 1
  srl $s2, $s2, 1    # s2 = L

  # step 6. L[31] = H[0]
  li $s6, 31  # s6 = 31
  extract_nth_bit($s7, $s1, $zero)  # s7 = H[0]
  insert_to_nth_bit($s2, $s6, $s7, $t9) # L[31] = H[0]
  #move $s2, $v0 # s2 = L = v0

  # step 7. H = H >> 1
  srl $s1, $s1, 1    # s1 = H

  # step 8. I = I + 1
  addi $s0, $s0, 1  # I++

  # step 9. I == 32 ? : end, goto 2
  beq $s0, 32, end_mul_unsigned  # (I==32)?:end,contiue;
  j for_loop_mul

  #--------------
end_mul_unsigned:
  move $v0, $s2 # $v0 : Lo part of result = s2 = L 
  move $v1, $s1 # $v1 : Hi part of result = s1 = H
  #restore RTE
  lw  $fp, 52($sp)
  lw  $ra, 48($sp)
  lw  $a0, 44($sp)
  lw  $a1, 40($sp)
  lw  $s0, 36($sp)
  lw  $s1, 32($sp)
  lw  $s2, 28($sp)
  lw  $s3, 24($sp)
  lw  $s4, 20($sp)
  lw  $s5, 16($sp)
  lw  $s6, 12($sp)
  lw  $s7,  8($sp)
  addi $sp, $sp, 52
  jr $ra
