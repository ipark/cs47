twos_complement:
  #store RTE - (2 + 1) * 4 = 12 bytes
  addi  $sp, $sp, -16
  sw  $fp, 16($sp)
  sw  $ra, 12($sp)
  sw  $a0,  8($sp) # first number to be 2's complement
  #--------------
  not $a0, $a0        # $a0 : inverted $a0; first #
  li $a1, 1           # $a1 : second #
  jal add_logical     # $a0 = ~$a0 + 1
  j end_twos_complement
  #--------------
end_twos_complement:
  #restore RTE
  lw  $fp, 16($sp)
  lw  $ra, 12($sp)
  lw  $a0,  8($sp)
  addi $sp, $sp, 16
  jr $ra
