twos_complement_64bit:
  #store RTE - (2 + 5) * 4 = 28 bytes
  addi  $sp, $sp, -32
  sw  $fp, 32($sp)
  sw  $ra, 28($sp)
  sw  $a0, 24($sp) # Lo of the number
  sw  $a1, 20($sp) # Hi of the number
  sw  $s0, 16($sp) # s0 <- a0
  sw  $s1, 12($sp) # s1 <- a1
  sw  $s2,  8($sp) # s3 <- v0 from LO add_logical with ~a0 + 1
  #--------------
  move $s0, $a0       # safe store of a0 to s0
  move $s1, $a1       # safe store of a1 to s1
  not $a0, $a0        # $a0: inverted $a0; 1st #
  li  $a1, 1          # $a1: 2nd # 
  jal add_logical     # LO result in $v0, $v1
  move $s2, $v0       # $s2: $v0 from LO ~$a0 + 1
  move $a0, $v1       # $a0: carryout from LO ~$a0 + 1; 1st #
  move $a1, $s1       # a1 <- s1
  not $a1, $a1        # $a1: inverted $a1; 2nd #
  jal add_logical     # HI result in $v0, $v1
  move $v1, $v0       # $v1 : Hi part of 2's complemented 64 bit
  move $v0, $s2       # $v0 : Lo part of 2's complemented 64 bit
  #--------------
end_twos_complement_64bit:
  #restore RTE
  lw  $fp, 32($sp)
  lw  $ra, 28($sp)
  lw  $a0, 24($sp)
  lw  $a1, 20($sp)
  lw  $s0, 16($sp)
  lw  $s1, 12($sp)
  lw  $s2,  8($sp)
  addi $sp, $sp, 32
  jr $ra
