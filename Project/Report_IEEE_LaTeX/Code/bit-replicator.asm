bit_replicator:
  #store RTE - (2 + 1) * 4 = 12 bytes
  addi  $sp, $sp, -16
  sw  $fp, 16($sp)
  sw  $ra, 12($sp)
  sw  $a0,  8($sp) # num to be 2's complement
  #--------------
  beq $a0, $zero, allZero
  li $v0, -1 # 0xFFFFFFFF  
  j end_bit_replicator
allZero:
  li $v0, 0  # 0x00000000
  #--------------
end_bit_replicator:
  #restore RTE
  lw  $fp, 16($sp)
  lw  $ra, 12($sp)
  lw  $a0,  8($sp)
  addi $sp, $sp, 16
  jr $ra
