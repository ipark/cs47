add_sub_logical:
  #store RTE - (2 + 12) * 4 = 56 bytes
  addi  $sp, $sp, -56
  sw  $fp, 56($sp)
  sw  $ra, 52($sp)
  sw  $a0, 48($sp) # first number
  sw  $a1, 44($sp) # second number
  sw  $a2, 40($sp) # mode
  sw  $s0, 36($sp) # I
  sw  $s1, 32($sp) # C
  sw  $s2, 28($sp) # a0[index]
  sw  $s3, 24($sp) # a1[index]
  sw  $s4, 20($sp) # a0(xor)a1
  sw  $s5, 16($sp) # a0&a1
  sw  $s6, 12($sp) # Y
  sw  $s7,  8($sp) # S
  addi  $fp, $sp, 56
  #--------------
  li $s0, 0     # s0 = I = 0 
  li $s7, 0     # s7 = S = 0 
  # s1 = C = a2[s0]
  extract_nth_bit($s1, $a2, $s0) 

for_loop_add_sub:
  # s2 = a0[s0]
  extract_nth_bit($s2, $a0, $s0) 
  # s3 = a1[s0]
  extract_nth_bit($s3, $a1, $s0) 
  # s4= A xor B
  xor $s4, $s2, $s3 
  # s5= A.B
  and $s5, $s2, $s3 
  # s6= Y = C xor (A xor B) 
  xor $s6, $s1, $s4 
  # t7=     C.(A xor B)
  and $t7, $s1, $s4 
  # s1= C = C.(A xor B) + A.B 
  or $s1, $t7, $s5  
  # $v0=$s7=S
  insert_to_nth_bit($v0, $s0, $s6, $t9) 
  addi $s0, $s0, 1  # I++
  # (I==32)?:end,contiue;
  blt $s0, 32, for_loop_add_sub  
  # upgrade add_sub_logical to return 
  # final carryout (C=s1) in $v1
  move $v1, $s1
  #--------------
end_add_sub_logical:
  #restore RTE
  lw  $fp, 56($sp)
  lw  $ra, 52($sp)
  lw  $a0, 48($sp)
  lw  $a1, 44($sp)
  lw  $a2, 40($sp)
  lw  $s0, 36($sp)
  lw  $s1, 32($sp)
  lw  $s2, 28($sp)
  lw  $s3, 24($sp)
  lw  $s4, 20($sp)
  lw  $s5, 16($sp)
  lw  $s6, 12($sp)
  lw  $s7,  8($sp)
  addi $sp, $sp, 56
  jr $ra
