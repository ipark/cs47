.text
.globl au_normal

au_normal:
  #store RTE - (2 + 3) * 4 = 20 bytes
  addi  $sp, $sp, -24
  sw  $fp, 24($sp)
  sw  $ra, 20($sp)
  sw  $a0, 16($sp) # first number
  sw  $a1, 12($sp) # second number
  sw  $a2,  8($sp) # operation code
  addi  $fp, $sp, 24

  beq $a2, '+', add 
  beq $a2, '-', sub
  beq $a2, '*', mul
  beq $a2, '/', div 

add:
  add $v0, $a0, $a1
  j end
sub:
  sub $v0, $a0, $a1
  j end

mul:
  mult $a0, $a1 # muliply signed intergers, resulting 64-bit
  mflo $v0 # move lower 32-bit from LO to $v0
  mfhi $v1 # from upper 32-bit from HI to $v1
  j end

div:
  div $a0, $a1 # $LO = $a0 / $a1; $HI = $a0 % $a1
  mflo $v0 # move from LO (quotinent) to $v0
  mfhi $v1 # move from HI (remainder) to $v1
  j end

end:
  #restore RTE
  lw  $fp, 24($sp)
  lw  $ra, 20($sp)
  lw  $a0, 16($sp)
  lw  $a1, 12($sp)
  lw  $a2,  8($sp)
  addi  $sp, $sp, 24
  jr $ra
