mul_signed:
  #store RTE - (2 + 10) * 4 = 48 bytes
  addi  $sp, $sp, -52
  sw  $fp, 52($sp)
  sw  $ra, 48($sp)
  sw  $a0, 44($sp) # $a0 : N1
  sw  $a1, 40($sp) # $a1 : N2
  sw  $s0, 36($sp) # N1 = a0 (reserved after mul_unsigned)
  sw  $s1, 32($sp) # N2 = a1 (reserved after mul_unsigned)
  sw  $s2, 28($sp) # 2's complement of N1 if N1 < 0 
  sw  $s3, 24($sp) # 2's complement of N2 if N2 < 0 
  sw  $s4, 20($sp) # Rlo of mul_unsigned
  sw  $s5, 16($sp) # Rhi of mul_unsigned
  sw  $s6, 12($sp) # s6 = a0[31]
  sw  $s7,  8($sp) # s7 = a1[31]
  addi  $fp, $sp, 52
  #--------------
  # step 1. N1 = $a0, N2 = $a1                                       
  move $s0, $a0 # s0 = N1
  move $s1, $a1 # s1 = N2

  # step 2. Make N1 two's complement if negative
  move $a0, $s0 # a0 = N1 to use twos_complement_if_neg
  jal twos_complement_if_neg  # v0 : 2's complement of a0
  move $s2, $v0 # s2 : 2's complement of N1

  # step 3. Make N2 two's complement if negative
  move $a0, $s1 # a0 = N2 to use twos_complement_if_neg
  jal twos_complement_if_neg  # v0 : 2's complement of a0
  move $s3, $v0 # s3 : 2's complement of N2

  # step 4. Call unsigned multiplication using N1, N2. 
  move $a0, $s2 # a0 = s2 = N1
  move $a1, $s3 # a1 = s3 = N2
  jal mul_unsigned  # v0: Lo, v1: Hi

  # step 5. Say the result is Rhi, Rlo
  move $s4, $v0 # Rlo of mul_unsigned
  move $s5, $v1 # Rhi of mul_unsigned

  # step 6. Extract $a0[31] and $a1[31] bits and xor between them. 
  li $t5, 31
  extract_nth_bit($s6, $s0, $t5)  # s6 = a0[31], where a0 = s0
  extract_nth_bit($s7, $s1, $t5)  # s7 = a1[31], where a1 = s1

  # step 7. The result of xor $a0[31] and $a1[31] is S.
  xor $t6, $s6, $s7

  # step 8. If S is 1, use the 'twos_complement_64bit' to determine 
  #         2's complement form of 64 bit number in Rhi, Rlo.
  # if S != 1, goto end
  bne $t6, 1, end_mul_signed 
  # else S == 1, convert to 2's complement of Rlo, Rhi
  move $a0, $s4 # $a0 : Lo of the number
  move $a1, $s5 # $a1 : Hi of the number
  jal twos_complement_64bit # 
  # $v0 : Lo part of 2's complemented 64 bit
  # $v1 : Hi part of 2's complemented 64 bit

  #--------------
end_mul_signed:
  #restore RTE
  lw  $fp, 52($sp)
  lw  $ra, 48($sp)
  lw  $a0, 44($sp)
  lw  $a1, 40($sp)
  lw  $s0, 36($sp)
  lw  $s1, 32($sp)
  lw  $s2, 28($sp)
  lw  $s3, 24($sp)
  lw  $s4, 20($sp)
  lw  $s5, 16($sp)
  lw  $s6, 12($sp)
  lw  $s7,  8($sp)
  addi $sp, $sp, 52
  jr $ra
