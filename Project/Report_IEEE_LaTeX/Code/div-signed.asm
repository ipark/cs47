div_signed:
  #store RTE - (2 + 10) * 4 = 48 bytes
  addi  $sp, $sp, -52
  sw  $fp, 52($sp)
  sw  $ra, 48($sp)
  sw  $a0, 44($sp) # $a0 : Dividend
  sw  $a1, 40($sp) # $a1 : Divisor
  sw  $s0, 36($sp) # N1 = a0
  sw  $s1, 32($sp) # N2 = a1
  sw  $s2, 28($sp) # 2's complement of N1 if N1 < 0 
  sw  $s3, 24($sp) # 2's complement of N2 if N2 < 0 
  sw  $s4, 20($sp) # Q
  sw  $s5, 16($sp) # R
  sw  $s6, 12($sp) # a0[31]
  sw  $s7,  8($sp) # a1[31]
  addi  $fp, $sp, 52
  #--------------

  # step 1. N1 = $a0, N2 = $a1
  move $s0, $a0 # s0 = N1 = a0
  move $s1, $a1 # s1 = N2 = a1
  
  # step 2. Make N1 two's complement if negative
  move $a0, $s0 # a0 = N1 to use twos_complement_if_neg
  jal twos_complement_if_neg  # v0 : 2's complement of a0
  move $s2, $v0 # s2 : 2's complement of N1
  
  # step 3. Make N2 two's complement if negative
  move $a0, $s1 # a0 = N2 to use twos_complement_if_neg
  jal twos_complement_if_neg  # v0 : 2's complement of a0
  move $s3, $v0 # s3 : 2's complement of N2

  # step 4. Call unsigned Division using N1, N2. 
  move $a0, $s2 # a0 = s2 = N1
  move $a1, $s3 # a1 = s3 = N2
  jal div_unsigned  # result in v0 : Quotient; v1 : Remainder

  # step 5. Say the result is Q and R
  move $s4, $v0  # s4 = v0 : Quotient
  move $s5, $v1  # s5 = v1 : Remainder
  
  # step 6. Determine sign S of Q
  # step 6A. Extract $a0[31] and $a1[31] bits and xor between them
  li $t5, 31
  extract_nth_bit($s6, $s0, $t5)  # s6 = a0[31], where a0 = s0
  extract_nth_bit($s7, $s1, $t5)  # s7 = a1[31], where a1 = s1

  # step 6B. The xor result is S.
  xor $t6, $s6, $s7

  # step 6C. If S is 1, use the 'twos_complement' for 2's complement of Q
  bne $t6, 1, nextR # is S != 1 (i.e. negative), convert to 2's complement of Q
  move $a0, $s4 # s4 = Quotient
  jal twos_complement_64bit 
  move $s4, $v0 # s4 = 2's complement form of Quotient

nextR:
  # step 7. Determine sign S of R
  # step 7A. Extract $a0[31] and assign this to S
  li $t5, 31
  extract_nth_bit($s6, $s0, $t5)  # s6 = a0[31], where a0 = s0
  insert_to_nth_bit($t7, $zero, $s6, $t9) # (t7 = S) = (s6 = a0[31])

  # step 7B. If S is 1, use the 'twos_complement' for 2's complement of R
  bne $t7, 1, end_div_signed # is S != 1 (i.e. negative), convert to 2's complement of Q
  move $a0, $s5 # s5  = Remainder
  jal twos_complement_64bit 
  move $s5, $v0 # s5 = 2's complement form of Remainder

  #--------------
#   $v0 : Quotient
#   $v1 : Remainder
end_div_signed:
  move $v0, $s4 # s4 = Q
  move $v1, $s5 # s5 = R
  #restore RTE
  lw  $fp, 52($sp)
  lw  $ra, 48($sp)
  lw  $a0, 44($sp)
  lw  $a1, 40($sp)
  lw  $s0, 36($sp)
  lw  $s1, 32($sp)
  lw  $s2, 28($sp)
  lw  $s3, 24($sp)
  lw  $s4, 20($sp)
  lw  $s5, 16($sp)
  lw  $s6, 12($sp)
  lw  $s7,  8($sp)
  addi $sp, $sp, 52
  jr $ra

