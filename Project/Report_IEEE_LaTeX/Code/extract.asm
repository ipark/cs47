.macro extract_nth_bit($regDestination, $regSrcBitPattern, $regBitPosition)
 # regDestination = regSrcBitPattern >> regBitPosition
 srlv $regDestination, $regSrcBitPattern, $regBitPosition 
 # regDestination = regDestination & 0x1 
 andi $regDestination, $regDestination, 0x1               
.end_macro

