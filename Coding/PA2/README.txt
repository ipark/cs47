Add the following two macros in this cs47_macro.asm
swap_hi_lo ($temp1, $temp2) : This macro swap content of HI and LO register using specified temporary registers. For example 'swap_hi_lo($t0, $t1)' will swap content of HI and LO using $t0 and $t1 register.
print_hi_lo ($strHi, $strEqual, $strComma, $strLo) : This macro prints content of HI and LO register. It expects string address for string 'Hi', string '=', string ',' and string 'Lo'. You can use any temporary register inside macro definition to bring data out from HI and LO registers.
Assemble and execute pa02.asm. This should create following sample output
Enter number for Hi ? 28
Enter number for Lo ? 56
Before swapping Hi = 28 , Lo = 56 After swapping Hi = 56 , Lo = 28

