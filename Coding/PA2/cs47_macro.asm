#<------------------ MACRO DEFINITIONS ---------------------->#
  # Macro : print_str
  # Usage: print_str(<address of the string>)
  .macro print_str($arg)
  li	$v0, 4      # System call code for print_str  
  la	$a0, $arg   # Address of the string to print
  syscall         # Print the string        
  .end_macro
  
  # Macro : print_int
  # Usage: print_int(<val>)
  .macro print_int($arg)
  li 	$v0, 1     # System call code for print_int
  li	$a0, $arg  # Integer to print
  syscall        # Print the integer
  .end_macro
  
  # Macro : exit
  # Usage: exit
  .macro exit
  li 	$v0, 10 
  syscall
  .end_macro
  
  # [CS47,01] PA01, Due 09/19/2018, INHEE PARK
  # Macro : read_int
  # Usage : read_int($t7)
  # Take integer input from STDIO into register $t7
  .macro read_int($reg)
  li $v0, 5       # syscall code 5 = read integer 
  syscall
  move $reg, $v0  # set the Integer from STDIO into register
  .end_macro
  
  # [CS47,01] PA01, Due 09/19/2018, INHEE PARK
  # Macro : print_reg_int
  # Usage : print_reg_int($t7)
  # Print integer value stored in register $t7 onto STDIO
  .macro print_reg_int($reg)
  li $v0, 1       # syscall code 1 = print_int
  move $a0, $reg  # set $a0 the Integer from register to print
  syscall
  .end_macro

  # [CS47,01] PA02, Due 09/25/2018, INHEE PARK
  # Macro : print_hi_lo
  # Usage : print_hi_lo($strHi, $strEqual, $strComma, $strLo)
  .macro print_hi_lo($strHi, $strEqual, $strComma, $strLo)
  mfhi $t1			        # To copy from Hi to specified register
  mflo $t2		          # To copy from Lo to specified register
  print_str($strHi)		  # Hi 
  print_str($strEqual)	# = 
  print_reg_int($t1)		# print integer from stored in the register
  print_str($strComma)	# ,
  print_str($strLo)  		# Lo 
  print_str($strEqual)	# = 
  print_reg_int($t2)		# print integer from stored in the register
  #syscall  			      # BE CAUTIOUS!!! THIS syscall resulted in print values twice 
  .end_macro


  # [CS47,01] PA02, Due 09/25/2018, INHEE PARK
  # Usage : swap_hi_lo ($t0, $t1) 
  .macro swap_hi_lo($temp1, $temp2)
  move $t0, $temp1		# $t0 <= $temp1	
  move $t1, $temp2		# $t1 =< $temp2 
  mfhi $t0			      # $t0 <= hi; copy From HI to $t0(reg)=HI
  mflo $t1			      # $t1 <= lo; copy From LO to $t1(reg)=LO
  mthi $t1			      # hi <= $t1; copy To HI from $t1(reg)=LO
  mtlo $t0			      # lo <= $t0; copy To LO from $t0(reg)=HI
  syscall
  .end_macro
