Enhance procedure 'printf' to handle escape character '\'. Anything after escape character losses its special meaning if any. For example "\\%s" should print '%s' on terminal instead of refereeing to corresponding string and print the string.
Assemble and execute printf_extension.asm. After the above enhancement, the program should run ok. This should create following sample output
My string is 'test printf' and integer is 16 My name is John Adams
I am 26 year old
I love to eat pizza
I will graduate in 2015 My name is %s
I am %d year old
I love to eat %s
I will graduate in %d
