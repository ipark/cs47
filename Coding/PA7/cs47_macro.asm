#<------------------ MACRO DEFINITIONS ---------------------->#
  # Macro : print_str
  # Usage: print_str(<address of the string>)
  .macro print_str($arg)
  li	$v0, 4      # System call code for print_str  
  la	$a0, $arg   # Address of the string to print
  syscall         # Print the string        
  .end_macro
  
  # Macro : print_int
  # Usage: print_int(<val>)
  .macro print_int($arg)
  li 	$v0, 1     # System call code for print_int
  li	$a0, $arg  # Integer to print
  syscall        # Print the integer
  .end_macro
  
  # Macro : exit
  # Usage: exit
  .macro exit
  li 	$v0, 10 
  syscall
  .end_macro
  
  # [CS47,01] PA01, Due 09/19/2018, INHEE PARK
  # Macro : read_int
  # Usage : read_int($t7)
  # Take integer input from STDIO into register $t7
  .macro read_int($reg)
  li $v0, 5       # syscall code 5 = read integer 
  syscall
  move $reg, $v0  # set the Integer from STDIO into register
  .end_macro
  
  # [CS47,01] PA01, Due 09/19/2018, INHEE PARK
  # Macro : print_reg_int
  # Usage : print_reg_int($t7)
  # Print integer value stored in register $t7 onto STDIO
  .macro print_reg_int($reg)
  li $v0, 1       # syscall code 1 = print_int
  move $a0, $reg  # set $a0 the Integer from register to print
  syscall
  .end_macro

  # [CS47,01] PA02, Due 09/25/2018, INHEE PARK
  # Macro : print_hi_lo
  # Usage : print_hi_lo($strHi, $strEqual, $strComma, $strLo)
  .macro print_hi_lo($strHi, $strEqual, $strComma, $strLo)
  mfhi $t1			        # To copy from Hi to specified register
  mflo $t2		          # To copy from Lo to specified register
  print_str($strHi)		  # Hi 
  print_str($strEqual)	# = 
  print_reg_int($t1)		# print integer from stored in the register
  print_str($strComma)	# ,
  print_str($strLo)  		# Lo 
  print_str($strEqual)	# = 
  print_reg_int($t2)		# print integer from stored in the register
  #syscall  			      # BE CAUTIOUS!!! THIS syscall resulted in print values twice 
  .end_macro


  # [CS47,01] PA02, Due 09/25/2018, INHEE PARK
  # Usage : swap_hi_lo ($t0, $t1) 
  .macro swap_hi_lo($temp1, $temp2)
  move $t0, $temp1		# $t0 <= $temp1	
  move $t1, $temp2		# $t1 =< $temp2 
  mfhi $t0			      # $t0 <= hi
  mflo $t1			      # $t1 <= lo
  mthi $t1			      # hi <= $t1 (now, Hi register has val form Lo)
  mtlo $t0			      # lo <= $t0 (now, Lo register has val from Hi)
  syscall
  .end_macro


  # [CS47,01] PA03, Due 09/26/2018, INHEE PARK
  # Macro : lwi
  # Usage: lwi($s1, 0x5a5a, 0xa5a5) 
  # Result: $s1 = 0x5a5aa5a5  = 1515890085
  # This macro takes two 16-bit immediate values $ui and $li and 
  # put them into higher bits and lower bits of $reg. 
  .macro lwi ($reg, $ui, $li) 
  lui $reg, $ui           # load higher 16-bit to $reg
  ori $reg, $reg, $li     # OR between (higher 16-bit from $reg) and (lower 16-bit from $li)  
  .end_macro


  # [CS47,01] PA04, Due 10/01/2018, INHEE PARK
  # Macro : push
  # Usage : push($s0) content of register $s0 into the stack.
  # Store data first and then decrease stack pointer
  .macro push($reg)
  sw $reg, 0x0($sp)
  addi $sp, $sp, -4
  .end_macro

  # [CS47,01] PA04, Due 10/01/2018, INHEE PARK
  # Macro : pop
  # Usage : pop($t0) content from stack and copy into register $t0.
  # First increase the stack pointer and then load data
  .macro pop($reg)
  addi $sp, $sp, +4
  lw $reg, 0x0($sp) 
  .end_macro
