.include "./cs47_macro.asm"

.data
msg1: .asciiz "Enter a number ? "
msg2: .asciiz "Factorial of the number is "
charCR: .asciiz "\n"

.text 0x00400004
.globl main
main:	print_str(msg1)
	read_int($t0)
	
# Write body of the iterative
# factorial program here
# Store the factorial result into 
# register $s0
#
# DON'T IMPLEMENT RECURSIVE ROUTINE 
# WE NEED AN ITERATIVE IMPLEMENTATION 
# RIGHT AT THIS POSITION. 
# DONT USE 'jal' AS IN PROCEDURAL /
# RECURSIVE IMPLEMENTATION.

# [CS47,01] PA05, Due 10/8/2018, INHEE PARK		
###########################################
move $t1, $t0  # $t1 = N (read_int($t0))
li $t2, 1      # $t2 = product (=1; init)
li $t3, 1      # $t3 = j (=1; loop index)
loop: 
bgt $t3, $t1, exit   # if j(t3)>N(t1), exit
mul $t2, $t2, $t3    # product(t2) *= j(t3)
move $s0, $t2        # score result to $s0
addi $t3, $t3, 1     # j++
j loop 
exit: 
###########################################
	print_str(msg2)
	print_reg_int($s0)
	print_str(charCR)
	
