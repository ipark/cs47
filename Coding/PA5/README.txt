Complete the Factorial_Iterative.asm to implement iterative factorial calculation. Assemble and execute Factorial_Iterative.asm This should create following sample output
Enter a number ? 10
Factorial of the number is 3628800
