#<------------------ MACRO DEFINITIONS ---------------------->#
  # Macro : print_str
  # Usage: print_str(<address of the string>)
  .macro print_str($arg)
  li	$v0, 4      # System call code for print_str  
  la	$a0, $arg   # Address of the string to print
  syscall         # Print the string        
  .end_macro
  
  # Macro : print_int
  # Usage: print_int(<val>)
  .macro print_int($arg)
  li 	$v0, 1     # System call code for print_int
  li	$a0, $arg  # Integer to print
  syscall        # Print the integer
  .end_macro
  
  # Macro : exit
  # Usage: exit
  .macro exit
  li 	$v0, 10 
  syscall
  .end_macro
  
  # [CS47,01] PA01, Due 09/19/2018, INHEE PARK
  # Macro : read_int
  # Usage : read_int($t7)
  # Take integer input from STDIO into register $t7
  .macro read_int($reg)
  li $v0, 5       # syscall code 5 = read integer 
  syscall
  move $reg, $v0  # set the Integer from STDIO into register
 .end_macro
  
  # [CS47,01] PA01, Due 09/19/2018, INHEE PARK
  # Macro : print_reg_int
  # Usage : print_reg_int($t7)
  # Print integer value stored in register $t7 onto STDIO
  .macro print_reg_int($reg)
  li $v0, 1       # syscall code 1 = print_int
  move $a0, $reg  # set $a0 the Integer from register to print
  syscall
  .end_macro
