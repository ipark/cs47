Extend cs47_macro.asm to include following two macros:
read_int($reg) : To read an integer value from STDIO into given register. For example read_int($t1) will wait for user input for an integer and will store it in register $t1.
print_reg_int($reg): To print integer value in given register on STDIO. For example print_reg_int($t1) will print integer value stored in $t1 register on STDIO.
Assemble pa01.asm (which includes cs47_macro.asm) and execute. The main program should create output on STDIO as following.
Please enter a number? 56 You have entered # 56
