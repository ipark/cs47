Complete the LCM_Recursive.asm to implement recursive LCM calculation. The following is the algorithm.
Call: lcm_recursive(m,n,m,n);
int lcm_recursive (int m, int n, int lcm_m, lcm_n) {
if (lcm_m == lcm_n) return lcm_m;
if (lcm_m > lcm_n) lcm_n = lcm_n + n;
else lcm_m = lcm_m + m;
return (lcm_recursive(m,n, lcm_m, lcm_n));
}
Assemble and execute LCM_Recursive.asm This should create following sample output
Enter a +ve number : 4
Enter another +ve number : 6 LCM of 4 and 6 is 12
