.include "./cs47_macro.asm"

.data
msg1: .asciiz "Enter a +ve number : "
msg2: .asciiz "Enter another +ve number : "
msg3: .asciiz "LCM of "
s_is: .asciiz "is"
s_and: .asciiz "and"
s_space: .asciiz " "
s_cr: .asciiz "\n"

.text
.globl main
main:
	print_str(msg1)
	read_int($s0)       # m
	print_str(msg2)
	read_int($s1)       # n
	
	move $v0, $zero     # initialize return value register $v0
	move $a0, $s0       # $a0=m
	move $a1, $s1       # $a1=n
	move $a2, $s0       # lcm_m
	move $a3, $s1       # lcm_n
	jal  lcm_recursive
	move $s3, $v0       # return from lcm_recursive
	
	print_str(msg3)     # "LCM of "
	print_reg_int($s0)  # m
	print_str(s_space)
	print_str(s_and)
	print_str(s_space)
	print_reg_int($s1)  # n
	print_str(s_space)
	print_str(s_is)
	print_str(s_space)
	print_reg_int($s3) # print return from lcm_recursive  
	print_str(s_cr)
	exit

#------------------------------------------------------------------------------
# Function: lcm_recursive 
# Argument:
#	      $a0 : +ve integer number m
#       $a1 : +ve integer number n
#       $a2 : temporary LCM by increamenting m, initial is m
#       $a3 : temporary LCM by increamenting n, initial is n
# Returns
#	      $v0 : lcm of m,n 
#
# Purpose: Implementing LCM function using recursive call.
# 
#------------------------------------------------------------------------------
#
# [CS47,01] PA06, Due 10/15/2018, INHEE PARK		
# Call: lcm_recursive(m,n,m,n);
# 
# int lcm_recursive (int m, int n, int lcm_m, lcm_n) 
# {
#   if (lcm_m == lcm_n) return lcm_m;    // base case
#   if (lcm_m > lcm_n)  lcm_n = lcm_n + n;
#   else                lcm_m = lcm_m + m;
#   return (lcm_recursive(m,n, lcm_m, lcm_n));
# }
#
lcm_recursive:
	# Store frame ##########
  addi $sp, $sp, -28 
  sw $fp, 28($sp)
  sw $ra, 24($sp)
  sw $a0, 20($sp) # $a0=m
  sw $a1, 16($sp) # $a1=n
  sw $a2, 12($sp) # $a2=lcm_m
  sw $a3,  8($sp) # $a3=lcm_n
  addi $fp, $sp, 28
	# Body ##########
  beq $a2, $a3, END  # lcm_m == lcm_n 
  bgt $a2, $a3, IF   # lcm_m > lcm_n
  blt $a2, $a3, ELSE # lcm_m < lcm_n
IF:
  add $a3, $a3, $a1  # lcm_n += n
  j RECUR
ELSE:
  add $a2, $a2, $a0  # lcm_m += m
  j RECUR
RECUR:
  jal lcm_recursive
  j RETURN
END:  
  move $v0, $a2      # return lcm_m;
  # Restore frame ##########
RETURN:
  lw $fp, 28($sp)
  lw $ra, 24($sp)
  lw $a0, 20($sp) # $a0=m
  lw $a1, 16($sp) # $a1=n
  lw $a2, 12($sp) # $a2=lcm_m
  lw $a3,  8($sp) # $a3=lcm_n
  addi $sp, $sp, 28
	jr $ra
