
.macro exit
li $v0, 10
syscall
.end_macro

.data
l: .asciiz "  ("
r: .asciiz ") "
cr: .asciiz "\n"
list: .word 3 2 5 4 1
num:  .word 5

.text
  j start

print_str_f: # {
	#store RTE - (2 + 1) * 4 = 12 bytes
	addi	$sp, $sp, -16
	sw	$fp, 16($sp)
	sw	$ra, 12($sp)
	sw	$v0, 8($sp)
	addi	$fp, $sp, 16


  ## body
  li $v0, 4
  syscall


	#restore RTE
	lw	$fp, 16($sp)
	lw	$ra, 12($sp)
	lw	$v0, 8($sp)
	addi	$sp, $sp, 16
	jr $ra
# }; end of print_str_f



# use one registers
#   $v0
print_reg_int_f: # {
	#store RTE - (2 + 1) * 4 = 12 bytes
	addi	$sp, $sp, -16
	sw	$fp, 16($sp)
	sw	$ra, 12($sp)
	sw	$v0, 8($sp)
	addi	$fp, $sp, 16


  ## body
  li $v0, 1
  syscall


	#restore RTE
	lw	$fp, 16($sp)
	lw	$ra, 12($sp)
	lw	$v0, 8($sp)
	addi	$sp, $sp, 16
	jr $ra
# }; end of print_reg_int_f



# $a0, $a1
# use $a2 as temporary register
addr4_f: # {
	#store RTE - (2 + 0) * 4 = 8 bytes
	addi	$sp, $sp, -12
	sw	$fp, 12($sp)
	sw	$ra, 8($sp)
	addi	$fp, $sp, 12


  ## body
  mul $t0, $a1, 4
  add $v0, $a0, $t0


	#restore RTE
	lw	$fp, 12($sp)
	lw	$ra, 8($sp)
	addi	$sp, $sp, 12
	jr $ra
# }; end of addr4_f



# $a0, $a1, $a2
# how many temporary registers
#   $a1
#   $s0
#   $s1
swap_f: # {
	#store RTE - (2 + 3) * 4 = 20 bytes
	addi	$sp, $sp, -24
	sw	$fp, 24($sp)
	sw	$ra, 20($sp)
	sw	$a1, 16($sp)
	sw	$s0, 12($sp)
	sw	$s1, 8($sp)
	addi	$fp, $sp, 24


  ## body
  jal addr4_f # use $a0, $a1
  move $s0, $v0
  # $a2 = $a1
  move $a1, $a2
  jal addr4_f # use $a0, $a1(=$a2)
  move $s1, $v0

  lw $t0, 0($s0)
  lw $t1, 0($s1)
  # swap
  sw $t0, 0($s1)
  sw $t1, 0($s0)


	#restore RTE
	lw	$fp, 24($sp)
	lw	$ra, 20($sp)
	lw	$a1, 16($sp)
	lw	$s0, 12($sp)
	lw	$s1, 8($sp)
	addi	$sp, $sp, 24
	jr $ra
# }; end of swap_f


# $a0: &a[0]
# $a1: n
# temporary registers
#   $a1
#   $s0: $a0
#   $s1: $a1
#   $s2: loop index, i
print_list_f: # {
	#store RTE - (2 + 4) * 4 = 24 bytes
	addi	$sp, $sp, -28
	sw	$fp, 28($sp)
	sw	$ra, 24($sp)
  # {; use registers as local variables
	sw	$a0, 20($sp)
	sw	$a1, 16($sp)
	sw	$s0, 12($sp)
	sw	$s1, 8($sp)
  # }; end of use registers as local variables
	addi	$fp, $sp, 28


  ## body
  move $s0, $a0
  move $s1, $a1 # save $a1 because it is used in addr4_f

  # a0: &a[0]
  # a1: num
  # s0 = a0
  # s1 = a1
  #
  # $s2: i
  li $s2, 0
for_loop_in_print_list: # {
  bge $s2, $s1, end_of_for_loop_in_print_list
  move $a0, $s0
  move $a1, $s2
  jal addr4_f
  lw $t2, 0($v0)

  la $a0, l
  jal print_str_f

  move $a0, $t2
  jal print_reg_int_f

  la $a0, r
  jal print_str_f

  addi $s2, $s2, 1
  j for_loop_in_print_list
# }; end of for-loop

end_of_for_loop_in_print_list:
  la $a0, cr
  jal print_str_f


	#restore RTE
	lw	$fp, 28($sp)
	lw	$ra, 24($sp)
  # {; restore registers
	lw	$a0, 20($sp)
	lw	$a1, 16($sp)
	lw	$s0, 12($sp)
	lw	$s1,  8($sp)
  # }; end of restore registers
	addi	$sp, $sp, 28
	jr $ra
# }; end of print_list_f



insertion_sort_f: # {
	#store RTE - (2 + 8) * 4 = 40 bytes
	addi	$sp, $sp, -44
	sw	$fp, 44($sp)
	sw	$ra, 40($sp)
  # {; use registers as local variables
	sw	$a1, 36($sp)
	sw	$a2, 32($sp)
	sw	$s0, 28($sp)
	sw	$s1, 24($sp)
	sw	$s2, 20($sp)
	sw	$s3, 16($sp)
	sw	$s4, 12($sp)
	sw	$s5,  8($sp)
  # }; end of use registers as local variables
	addi	$fp, $sp, 44


  ## body
  #
  # for (int i = 0; i < n; ++i) {
  #   int j = i;
  #   while (j > 0 && a[j-1] > a[j]) {
  #     int temp = a[j];
  #     a[j] = a[j-1];
  #     a[j-1] = temp;
  #     --j;
  #   }
  # }

  # two functions (addr4_f, swap_f) use $a0, $a1, $a2, and
  # we must keep $a1 (num)
  move $s0, $a1 # use $s0 instead of $a1

  # local variables
  # s0 = num // constant
  # s1 = i
  # s2 = j
  # s3 = j - 1
  # save $a1, $a2
  # s4 = &a[j]
  # s5 = a[j]
  # s6 = &a[j-1]
  # s7 = a[j-1]

  li $s1, 0 # loop index, i
for_loop_in_insertion_sort: # {
  bge $s1, $s0, end_of_for_loop_in_insertion_sort

  move $s2, $s1 # loop index, j
while_loop: # {
  beq $s2, $zero, incr_i
  addi $s3, $s2, -1 # s3 = j - 1

  # &a[j], &a[j-1]
  move $a1, $s2
  jal addr4_f
  move $s4, $v0 # s4 = &a[j]
  lw $s5, 0($v0) # s5 = *s4 = a[j]
  move $a1, $s3
  jal addr4_f
  # v0 = &a[j-1]
  lw $t7, 0($v0) # t7 = *v0 = a[j-1]

  ble $t7, $s5, incr_i

  # swap a[j] and a[j-1]
  sw $t7, 0($s4)
  sw $s5, 0($v0)

  addi $s2, $s2, -1 # --j
  j while_loop
# }; end of while_loop
incr_i:
  addi $s1, $s1, 1 # ++i
  j for_loop_in_insertion_sort
# }; end of for-loop

end_of_for_loop_in_insertion_sort:

	#restore RTE
	lw	$fp, 44($sp)
	lw	$ra, 40($sp)
  # {; restore registers
	lw	$a1, 36($sp)
	lw	$a2, 32($sp)
	lw	$s0, 28($sp)
	lw	$s1, 24($sp)
	lw	$s2, 20($sp)
	lw	$s3, 16($sp)
	lw	$s4, 12($sp)
	lw	$s5,  8($sp)
  # }; end of restore registers
	addi	$sp, $sp, 44
	jr $ra
# }; end of insertion_sort_f



start:
  la $s0, list       # t0<-address of list
  la $s1, num
  lw $s2, 0($s1)

  # print list before sort
  move $a0, $s0
  move $a1, $s2
  jal print_list_f

  # sort
# li $a1, 2
# li $a2, 3
# jal swap_f
  move $a0, $s0
  move $a1, $s2
  jal insertion_sort_f

  # print list after sort
  move $a1, $s2
  jal print_list_f

end:
  exit
