public class insertionSort {
  public static void main(String[] args) {
    int[] a = {3, 2, 5, 4, 1, 1, 1};
    for (int i = 0; i < a.length; i++) {
      int j = i;
      while (j > 0 && a[j] < a[j - 1]) {
        int tmp = a[j];
        a[j] = a[j - 1];
        a[j - 1] = tmp;
        j--;
      }
    }
    for (int i = 0; i < a.length; i++)
      System.out.print(a[i] + " ");
    System.out.println();
  }
}
