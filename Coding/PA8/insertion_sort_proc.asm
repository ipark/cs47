.text
#-------------------------------------------
# Procedure: insertion_sort
# Argument: 
#	$a0: Base address of the array
# $a1: Number of array element
# Return:
#       None
# Notes: Implement insertion sort, base array 
#        at $a0 will be sorted after the routine
#	       is done.
#-------------------------------------------

#############################################
# [CS47,01] PA08, Due 10/29/2018, INHEE PARK		
#
# for (int i = 0; i < a.length; i++) {
#   int j = i;
#   while (j > 0 && a[j] < a[j - 1]) {
#     int tmp = a[j];
#     a[j] = a[j - 1];
#     a[j - 1] = tmp;
#     j--;
#   }
# }
#############################################

insertion_sort: # {
	#store RTE - (2 + 10) * 4 = 48 bytes&
	addi	$sp, $sp, -52
	sw	$fp, 52($sp)
	sw	$ra, 48($sp)
	sw	$a0, 44($sp) # $a0: Base address of the array
	sw	$a1, 40($sp) # $a1: Number of array element
	sw	$s0, 36($sp) # temporary for $a1
	sw	$s1, 32($sp) # i
	sw	$s2, 28($sp) # j 
	sw	$s3, 24($sp) # j-1
	sw	$s4, 20($sp) # &a[j]
	sw	$s5, 16($sp) # *a[j]
	sw	$s6, 12($sp) # &a[j-1]
	sw	$s7,  8($sp) # *a[j-1]
	addi	$fp, $sp, 52

  #####CRUCIAL 
  # I used addr4_f function, where uses $a0 and $a1,
  # thus here store $a1 in a temporary register $s0
  move $s0, $a1
  li $s1, 0               # i = 0 (s1 = i)
  # for (int i = 0; i < a.length; i++) {
for_loop_in_insertion_sort: # {
  bge $s1, $s0, end_of_for_loop_in_insertion_sort # i >= a.length(a1) goto end
  move $s2, $s1           # j = i (s2 = s1)
while_loop: # { 
  beq $s2, $zero, incr_i  # if j == 0 go to incr_i
  addi $s3, $s2, -1       # s3 = j-1 

  move $a1, $s2           # to use addr4_f ($a0: starting addr, $a1:j)
  jal addr4_f             # v0 = &a[j]
  move $s4, $v0           # s4 = v0 = &a[j]
  lw $s5, 0($v0)          # s5 = *a[j]

  move $a1, $s3           # to use addr4_f ($a0: starting addr, $a1:j-1)
  jal addr4_f             # v0 = &a[j-1]
  move $s6, $v0           # s6 = v0 = &a[j-1]
  lw $s7, 0($v0)          # s7 = *a[j-1]

  bge $s5, $s7, incr_i    # if (a[j] >= a[j-1]) goto incr_i
  # swap a[j] and a[j-1]
  sw $s5, 0($s6)          # *a[j]->&a[j-1]
  #sw $s5, 0($s4)          # *a[j-1]->&a[j] <---typo, should be $s7, not $s5
  sw $s7, 0($s4)          # *a[j-1]->&a[j]

  addi $s2, $s2, -1 # j--
  j while_loop
# }; end of while_loop
incr_i:
  addi $s1, $s1, 1 # i++
  j for_loop_in_insertion_sort
# }; end of for-loop

end_of_for_loop_in_insertion_sort:

	#restore RTE
	lw	$fp, 52($sp)
	lw	$ra, 48($sp)
	lw	$a0, 44($sp)
	lw	$a1, 40($sp)
	lw	$s0, 36($sp)
	lw	$s1, 32($sp)
	lw	$s2, 28($sp)
	lw	$s3, 24($sp)
	lw	$s4, 20($sp)
	lw	$s5, 16($sp)
	lw	$s6, 12($sp)
	lw	$s7,  8($sp)
	addi	$sp, $sp, 52
	jr $ra

addr4_f: # {
	#store RTE - (2 + 0) * 4 = 8 bytes
	addi	$sp, $sp, -12
	sw	$fp, 12($sp)
	sw	$ra, 8($sp)
	addi	$fp, $sp, 12

  ## body
  mul $t0, $a1, 4
  add $v0, $a0, $t0

	#restore RTE
	lw	$fp, 12($sp)
	lw	$ra, 8($sp)
	addi	$sp, $sp, 12
	jr $ra
# }; end of addr4_f
