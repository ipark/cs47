Instructions:
Download PA08.zip in a directory.
Unzip the source codes.
Assemble and execute insertion_sort.asm This should create following sample output:
**** FAILED [4/10] ****
Complete insertion sort implementation in insertion_sort_proc.asm.
Assemble and execute insertion_sort.asm This should create following sample output:
**** PASSED [10/10] ****
Upload updated insertion_sort_proc.asm (do not change the file name). Do not upload any other file.
An outline of the insertion sort is as following (from Wikipedia on insertion sort)
           
for i ← 1 to length(A) j←i
  while j > 0 and A[j-1] > A[j] 
    swap A[j] and A[j-1] j←j - 1
  end while
end for
