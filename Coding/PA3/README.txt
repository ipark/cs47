Add the following macro in this cs47_macro.asm
lwi ($reg, $ui, $li) : This macro takes two 16­bit immediate values $ui and $li and put them into higher bits and lower bits of $reg. For example lwi($s1, 0x5a5a, 0xa5a5) will place a value 0x5a5aa5a5 into $s1 register.
Assemble and execute pa03.asm. This should create following sample output Register S1 contains integer number 1515890085
