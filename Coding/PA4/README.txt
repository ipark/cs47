Add the following two macros in this cs47_macro.asm
push($reg) : Push data in register $reg into stack.For example push($s0) will push the content of register $s0 into the stack.
pop($reg) : Pop from stack into register $reg. For example pop($t0) will pop content from stack and copy into register $t0.
Assemble and execute pa05.asm. This should create following sample output
Pushing ­> 16 | 12816 | 32 | 1985229328 Popping ­> 1985229328 | 32 | 12816 | 16
